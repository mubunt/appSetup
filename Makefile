#------------------------------------------------------------------------------
# Copyright (c) 2016-2018, Michel RIZZO (the 'author' in the following)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: appSetup
# A graphic utility to generate self-extractable archives
#-------------------------------------------------------------------------------
MUTE			= @
OPTIM_DEBUG		= -O0 -g
OPTIM_RELEASE	= -O2
STRIP_DEBUG		= :
STRIP_RELEASE	= strip

OPTIM 			= "$(OPTIM_DEBUG)"
STRIP 			= $(STRIP_DEBUG)
#-------------------------------------------------------------------------------
VERSION			= $(shell cat VERSION)
TAG 			= "Version $(VERSION) - $(shell LC_ALL=en_EN.UTF-8 LANG=en_EN.utf8 date '+%B %Y')"
GIT 			= git
#-------------------------------------------------------------------------------
help:
	@echo
	@echo ------------------
	@echo Available targets:
	@echo ------------------
	@echo
	@echo "	$(MAKE) all ........... Build project"
	@echo "	$(MAKE) clean ......... Clean project"
	@echo "	$(MAKE) install ....... Install project outputs"
	@echo "	$(MAKE) cleaninstall .. Remove installed project outputs"
	@echo "	$(MAKE) commit ........ (GIT) Commit and push new version"
	@echo "	---------------------------------------------------------"
	@echo "	$(MAKE) release ....... Release project"
	@echo
#-------------------------------------------------------------------------------
all:
	$(MUTE)cd  appSetup_c; $(MAKE) --no-print-directory $@ MUTE=$(MUTE) VERSION=$(VERSION) OPTIM=$(OPTIM) STRIP=$(STRIP); cd ..
	$(MUTE)cd  appSetup_java; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
clean:
	$(MUTE)cd  appSetup_c; $(MAKE) --no-print-directory $@ MUTE=$(MUTE) VERSION=$(VERSION) OPTIM=$(OPTIM) STRIP=$(STRIP); cd ..
	$(MUTE)cd  appSetup_java; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
	$(MUTE)cd  appSetup_bin; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..
install cleaninstall:
	$(MUTE)if [ "${INSTALLDIR}" = "" ]; then echo "ERROR: Environment variable or parameter INSTALLDIR not set"; \
	else cd appSetup_bin; $(MAKE) --no-print-directory $@ MUTE=$(MUTE); cd ..; \
	fi
commit:
	$(GIT) commit -m $(TAG)
	$(GIT) tag -a v$(VERSION) -m $(TAG)
	$(GIT) push
	$(GIT) push --tags
release:
	$(MUTE)$(MAKE) --no-print-directory clean MUTE=$(MUTE)
	$(MUTE)$(MAKE) --no-print-directory all MUTE=$(MUTE) VERSION=$(VERSION) OPTIM="$(OPTIM_RELEASE)" STRIP=$(STRIP_RELEASE)
	$(MUTE)$(MAKE) --no-print-directory cleaninstall INSTALLDIR=$(INSTALLDIR) MUTE=$(MUTE)
	$(MUTE)$(MAKE) --no-print-directory install INSTALLDIR=$(INSTALLDIR) MUTE=$(MUTE)
#-------------------------------------------------------------------------------
.PHONY: all clean install cleaninstall astyle commit release help
#-------------------------------------------------------------------------------
