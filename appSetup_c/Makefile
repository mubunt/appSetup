#-------------------------------------------------------------------------------
# Copyright (c) 2016-2018, Michel RIZZO (the 'author' in the following)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: appSetup
# A graphic utility to generate self-extractable archives
#-------------------------------------------------------------------------------
BINJAR			= ../appSetup_bin
PROGRAM			= $(BINJAR)/appSetup
CMDLINE			= appSetup_cmdline
SOURCES			= appSetup.c $(CMDLINE).c
GGO				= appSetup.ggo

APPSETUPEMBD	= $(notdir $(shell ls $(BINJAR)/appSetupEmbd.*))
COMMONSCLI		= $(notdir $(shell ls $(BINJAR)/commons-cli-*))
COMMONSIO		= $(notdir $(shell ls $(BINJAR)/commons-io-*))
SWT				= $(notdir $(shell ls $(BINJAR)/swt-*))

CC_JARS			= -DAPPSETUPEMBD="\"$(APPSETUPEMBD)\"" -DCOMMONSCLI="\"$(COMMONSCLI)\"" -DCOMMONSIO="\"$(COMMONSIO)\"" -DSWT="\"$(SWT)\""
#-------------------------------------------------------------------------------
OBJDIR			= ../linux
OBJECTS			= $(addprefix $(OBJDIR)/,$(SOURCES:.c=.o))
DEPDIR			= $(OBJDIR)/deps
#-------------------------------------------------------------------------------
CC				= gcc
MKDIR			= mkdir -p
RM				= rm -f
RMDIR			= rm -fr
GENGETOPT		= gengetopt
INSTALL			= install -p -v -D
#-------------------------------------------------------------------------------
CCFLAGS			=  $(CC_JARS) -m64 $(OPTIM) -I. -I.. -I$(OBJDIR) -W -Wall -Wextra -Wno-unused -Wconversion -Wwrite-strings -Wstack-protector --std=c99 -pedantic -D_GNU_SOURCE
DEPFLAGS		= -MT $@ -MMD -MP -MF $(DEPDIR)/$(notdir $*).Td
LDFLAGS			= -m64 ${OPTIM}
POSTCOMPILE		= mv -f $(DEPDIR)/$(notdir $*).Td $(DEPDIR)/$(notdir $*).d
# To compensate a generation error of Gengetopt
POSTGENGOPT		= mv $*.c $*.tmp; \
				  cat $*.tmp | sed -e \
				  "s/my_argv_arg = (char \*\*) malloc((my_argc+1) \* sizeof(char \*));/my_argv_arg = (char **) malloc((unsigned int) (my_argc+1) * sizeof(char *));/" \
				  > $*.c; \
            	  $(RM) $*.tmp

XVERSION		= "- Copyright (c) 2016-2018, Michel RIZZO. All Rights Reserved.\n$(notdir $(PROGRAM)) - Version $(VERSION)"
#-------------------------------------------------------------------------------
all: $(DEPDIR) $(OBJDIR)/$(CMDLINE).c $(OBJDIR)/$(CMDLINE).h $(PROGRAM)
clean:
	@echo Removing all generated files
	$(MUTE)$(RMDIR) $(OBJDIR)
#-------------------------------------------------------------------------------
$(PROGRAM): $(OBJECTS)
	@echo Linking $@
	$(MUTE)$(CC) $(LDFLAGS) -o $@ $(OBJECTS)
	$(MUTE)$(STRIP) $@
$(DEPDIR):
	$(MUTE)$(MKDIR) $(DEPDIR)
$(OBJDIR)/%.o: %.c
$(OBJDIR)/%.o: %.c $(DEPDIR)/%.d
	@echo Compiling $@
	$(MUTE)$(CC) $(DEPFLAGS) $(CCFLAGS) -o $@ -c $(addsuffix .c,$(basename $(notdir $@)))
	$(MUTE)$(POSTCOMPILE)
$(OBJDIR)/$(CMDLINE).o: $(OBJDIR)/$(CMDLINE).c $(OBJDIR)/$(CMDLINE).h $(DEPDIR)/$(CMDLINE).d
	@echo Compiling $@
	$(MUTE)$(CC) $(DEPFLAGS) $(CCFLAGS) -o $@ -c $(addsuffix .c,$(basename $@))
	$(MUTE)$(POSTCOMPILE)
$(OBJDIR)/$(CMDLINE).c $(OBJDIR)/$(CMDLINE).h: $(GGO)
	@echo Generating $*
	$(MUTE)$(GENGETOPT) --conf-parser --set-package=$(notdir $(basename $(PROGRAM))) --set-version=$(XVERSION) --func-name=cmdline_parser_appSetup --file-name=$(OBJDIR)/$(CMDLINE) < $<
	$(MUTE)$(POSTGENGOPT)
$(DEPDIR)/%.d: ;
#-------------------------------------------------------------------------------
.PRECIOUS: $(DEPDIR)/%.d
.PHONY: all clean
#-------------------------------------------------------------------------------
-include $(patsubst %,$(DEPDIR)/%.d,$(basename $(SOURCES)))
#-------------------------------------------------------------------------------

