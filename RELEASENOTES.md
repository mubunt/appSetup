# RELEASE NOTES: *appSetup*, A graphic utility to generate self-extractable archives.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.8.3 - Build 62 - September, 2018**
  - Some omissions... again.

- **Version 1.8.2 - Build 62 - September, 2018**
  - Some omissions...

- **Version 1.8.1 - Build 62 - September, 2018**
  - Updated Release Notes file.
  - Removed generated jar files on *clean*.

- **Version 1.8.0 - Build 62 - September, 2018**
  - Automated, as much as possible, the management of java libraries to facilitate their update.
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.
  - Added tagging of new release.
  - Moved from GPL v2 to GPL v3.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Improved version identification and optimization flags in Makefiles

- **Version 1.7.1 - Build 61 - August, 2018**
  -  Fixed typo in C driver file ("appSetupEmdbd.jar" instead of "appSetupEmbd.jar").
  -  Fixed typo in "appSetupEmbd.mk" file ("commons-cli-1.4jar" instead of "commons-cli-1.4.jar").
  -  Move to SWT 4.8. Initially was 4.6.3.

- **Version 1.7.0 - Build 60 - May, 2017**
  -  Added ".comment" file in each directory for 'yaTree' utility.

- **Version 1.6.1 - Build 60 - April, 2017**
  -  Move to SWT 4.6.3. Initially was 4.6.2.
  -  Move to Commons CLI 1.4 Initially was 1.3.1.

- **Version 1.6.0 - Build 60 - January, 2017**
  - Moved from ECLIPSE Neon.1 Release (4.6.1)  to IntelliJ IDEA 2016.3.2.

- **Version 1.5.2 - Build 55 - October, 2016**
  - Simplified 'index.html' to avoid redundancy with 'README'.
  - Added 'index.html' and made the changes resulting.
  - Added 'README.md'. Fixed typo in 'Makefile'.

- **Version 1.5.1 - September, 2016**
  -  Move SWT from 4.5 to 4.6.1.

- **Version 1.5.0 - September, 2016**
  -  Change source tree structure to comply with other developments.

- **Version 1.4.0 - September, 2016**
  -   Improved source readibilty by merging 2 classes (TarBall and UserApplication) then by creating one class per type of file to manage.
  -  Starting validation campaign....

- **Version 1.3.0 - September, 2016**
  -  Added a "check" button allowing to check the completeness of the "introduction" set of files.

- **Version 1.2.0 - September, 2016**
  -  HTML introduction file can reference CSS files, images, icons, etc. provided they belong to a sub-folder of the folder specified for the introduction file. Example: if '/a/b/c is the path of the 'introduction' file, then it can reference '/a/b/c/images/A.png' but not '/a/b/images/B.png'. These files have to be specified in a new '*Files required to Introduction file*' field, as a column at a rate of one file per line.

- **Version 1.1.0 - September, 2016**
  -  Drastic time optimization of the package installation at the expense of its size.

- **Version 1.0.0 - September, 2016**
  -  First release
