#------------------------------------------------------------------------------
# Copyright (c) 2016-2018, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: appSetup
# A graphic utility to generate self-extractable archives
#-------------------------------------------------------------------------------
APPLICATION	= appSetup
PROGRAM 	= appSetup

BINJAR		= ../$(APPLICATION)_bin
IDEA_PRJ	= $(PROGRAM)

COMMONSCLI	= $(notdir $(shell ls $(BINJAR)/commons-cli-*))
COMMONSIO	= $(notdir $(shell ls $(BINJAR)/commons-io-*))
SWT			= $(notdir $(shell ls $(BINJAR)/swt-*))

SOURCES		= $(IDEA_PRJ)/src/appSetup.java \
			  $(IDEA_PRJ)/src/appSetup_Free.java \
			  $(IDEA_PRJ)/src/appSetup_Introduction.java \
			  $(IDEA_PRJ)/src/appSetup_Jars.java \
			  $(IDEA_PRJ)/src/appSetup_Licenses.java \
			  $(IDEA_PRJ)/src/appSetup_Properties.java \
			  $(IDEA_PRJ)/src/appSetup_UserApp.java \
			  $(IDEA_PRJ)/src/appSetupBuildInfo.java \
			  $(IDEA_PRJ)/src/appSetupIntroModif.java \
			  $(IDEA_PRJ)/src/appSetupProperties.java \
			  $(IDEA_PRJ)/src/appSetupSelfExtractableArchive.java \
			  $(IDEA_PRJ)/src/appSetupShellCmd.java \
			  $(IDEA_PRJ)/src/appSetupUI.java
#-------------------------------------------------------------------------------
IDEA_BIN	= $(IDEA_PRJ)/out/production/$(IDEA_PRJ)
JARFILE		= $(BINJAR)/$(PROGRAM).jar
CLASSES 	= $(addprefix $(IDEA_BIN)/,$(notdir $(SOURCES:.java=.class)))
MANIFESTTMP	= manifest.txt
#-------------------------------------------------------------------------------
RM			= rm -f
RMDIR		= rm -fr
MKDIR		= mkdir -p
JC 			= javac
JCFLAGS		= -d $(IDEA_BIN) -cp "$(BINJAR)/*"
JAR 		= jar
JARFLAGS	= cmf

.SUFFIXES: .java .class
#-------------------------------------------------------------------------------
$(CLASSES):
	@echo $(PROGRAM): Compiling java sources...
	$(MUTE)$(MKDIR) $(IDEA_BIN)
	$(MUTE)$(JC) $(JCFLAGS) $(SOURCES)
$(JARFILE): $(CLASSES)
	$(MUTE)cd $(IDEA_BIN); \
		echo $(PROGRAM): Creating MANIFEST source file...; \
		echo "Manifest-Version: 1.0" > $(MANIFESTTMP); \
		echo "Class-Path: $(COMMONSCLI) $(COMMONSIO) $(SWT)" >> $(MANIFESTTMP); \
		echo "Main-Class: $(PROGRAM)" >> $(MANIFESTTMP); \
		echo "" >> $(MANIFESTTMP); \
		echo $(PROGRAM): Creating JAR file $(JARFILE)...; \
		$(JAR) $(JARFLAGS) $(MANIFESTTMP) ../../../../$(JARFILE) *.class; \
		$(RM) $(MANIFESTTMP)
#-------------------------------------------------------------------------------
all: $(JARFILE)
clean:
	@echo $(PROGRAM): Removing CLASS files...
	$(MUTE)$(RMDIR) $(IDEA_PRJ)/out
#-------------------------------------------------------------------------------
.PHONY: all clean
#-------------------------------------------------------------------------------
