//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

class appSetupShellCmd {
	private static final String[] du_cmd		= {
			"du",
			"--summarize",
			"--bytes"
	};
	private static final String tar_cmd = "tar --gzip --directory %s --create --file %s %s";
	private static final String untar_cmd = "tar --gunzip --extract --directory %s --file %s";
	private static final String tardir_cmd = "tar --gzip --create --file %s .";
	private static final String md5_cmd	= "md5sum %s";
	private static final String dd_cmd = "dd if=%s of=%s ibs=" + String.valueOf(appSetup.ddBlockSize) + " count=%d conv=sync";
	//--------------------------------------------------------------------------
	static String DU(String path) {
		String[] cmd = new String[du_cmd.length + 1];
		System.arraycopy(du_cmd, 0, cmd, 0, du_cmd.length);
		//for (int i=0; i < du_cmd.length; i++) cmd[i] = du_cmd[i];
		cmd[du_cmd.length] = path;
		String res = executeCommand(cmd);
		int n = res.indexOf("\t");
		if (n == -1) appSetup.Error("INTERNAL ERROR - Surprising result of the command '" + 
				cmd[0] +  " " + cmd[1] +  " " + cmd[2] +  " " + cmd[3] + "'");
		return res.substring(0 , n);
	}
	//--------------------------------------------------------------------------
	static void TAR(String tarball, String directory, String files) {
		executeCommand(String.format(tar_cmd, directory, tarball, files));
	}
	//--------------------------------------------------------------------------
	static void UNTAR(String tarball, String directory) {
		executeCommand(String.format(untar_cmd, directory, tarball));
	}
	//--------------------------------------------------------------------------
	static void TARDIR(String tarball, String directory) {
		List<String> cmds = new ArrayList<>();

		cmds.add("#!/bin/bash");
		cmds.add(String.format("cd %s", directory.replace(" ", "\\ ")));
		cmds.add(String.format(tardir_cmd, tarball));
		cmds.add("exit $?");

		String name = appSetup.tmpdir + appSetup.slash + appSetup.prefix + appSetup.suffixShell;
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(name), "utf-8"));
			for (String l : cmds) writer.write(l + appSetup.eol);
		} catch (IOException e) { appSetup.Error("Write error: " + e.getMessage());
		} finally {
			try {
				assert writer != null;
				writer.close(); } catch (Exception ignored) { }
		}
		File tmpf = new File(name);
		if (! tmpf.setExecutable(true))
			appSetup.Error("Cannot set file '" + tmpf.getPath() + "' executable");
		executeCommand(name);
	}
	//--------------------------------------------------------------------------
	static void DD(String paddedFile, String file, long size) {
		executeCommand(String.format(dd_cmd, file, paddedFile, computeNumberBlocks(size)));
	}
	//--------------------------------------------------------------------------
	static String getMD5checksum(String file) {
		String cmd = String.format(md5_cmd, file);
		String res = executeCommand(cmd);
		return res.substring(0 , res.indexOf(' '));
	}
	//--------------------------------------------------------------------------
	private static String executeCommand(String command) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = reader.readLine())!= null)
				if (line.length() != 0) output.append(line);
		} catch (Exception e) { appSetup.Error("Execution error: " + e.getMessage()); }
		return output + "";
	}

	private static String executeCommand(String[] command) {
		StringBuffer output = new StringBuffer();
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = reader.readLine())!= null)
				if (line.length() != 0) output.append(line);
		} catch (Exception e) { appSetup.Error("Execution error: " + e.getMessage()); }
		return output + "";
	}
	//--------------------------------------------------------------------------
	static long computeNumberBlocks(long size) {
		long $ = size / appSetup.ddBlockSize;
		if (size % appSetup.ddBlockSize != 0) $ += 1;
		return $;
	}
}
//==============================================================================
