//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
class appSetup_UserApp {
    private static String name = "";	// Name of the application as specified by user
    private static String nameApplicationFile = "";	// Internal name of application
    private static String paddedNameApplicationFile = "";
    private static String checksumApplicationFile = "";
    private static long sizeApplicationFile;

    private static String version = "";
    private static String date = "";
    private static long size;
    private static String path = "";

    private static String startupCommand = "";
    //--------------------------------------------------------------------------
    static String getName() { return name; }
    static void putName(String applicationname) { name = applicationname; }

    static String getNameApplication() { return nameApplicationFile; }
    static void putNameApplication() {
        nameApplicationFile = appSetup.nameApplicationFile +  appSetup.suffix_tar +  appSetup.suffix_gzip;
        paddedNameApplicationFile =  appSetup.prefix_padded + nameApplicationFile;
    }

    static String getPaddedNameApplication() { return paddedNameApplicationFile; }

    static String getChecksumApplication() { return checksumApplicationFile; }
    static void putChecksumApplication(String checksum) { checksumApplicationFile = checksum; }

    static long getSizeApplication() { return sizeApplicationFile; }
    static void putSizeApplication(long bytes) { sizeApplicationFile = bytes; }
    //--------------------------------------------------------------------------
    static String getVersion() { return version; }
    static void putVersion(String applicationvers) { version = applicationvers; }

    static String getDate() { return date; }
    static void putDate(String applicationdate) { date = applicationdate; }

    static long getSize() { return size; }
    static void putSize(long applicationsize) { size = applicationsize; }

    static String getPath() { return path; }
    static void putPath(String applicationpath) { path = applicationpath; }

    static String getStartup() { return startupCommand; }
    static void putStartup(String startup) { startupCommand =startup; }
}
//==============================================================================