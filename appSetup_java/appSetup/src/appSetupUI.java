//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class appSetupUI {
	private static Text textName;
	private static Text textVersion;
	private static Text textDate;
	private static Text textPath;
	private static Text textStartup;
	private static Text txtIntroductionFile;
	static Text txtIntroReferencedFiles;
	private static Text txtFreeLicensesFile;
	private static Text txtLicensesFile;
	private static Button cancel;
	private static Button introCheck;
	static Display display;
	private static Shell shell;
	private static ProgressBar bar;
	private static Composite topLevel;
	private static String dirforresearch = appSetup.homeDirectory;
	static Image oldBackImage;
	//--------------------------------------------------------------------------
	static final Font fontTitle = new Font(Display.getDefault(), "Arial", 20, SWT.BOLD);
	static final Font fontBuild = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	static final Font fontLabel = new Font(Display.getDefault(), "Arial", 10, SWT.NORMAL);
	static final Font fontButton = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	//--------------------------------------------------------------------------
	static final ModifyListener modifytxtIntroReferencedFiles	= new appSetupIntroModif();
	//--------------------------------------------------------------------------
	static void initUserInterface() {
		display = Display.getCurrent();
		shell = new Shell(display, SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.BORDER);
		shell.setText(appSetup.sName
				+ " - Build: " + appSetupBuildInfo.getNumber()
				+ " - Date: " + appSetupBuildInfo.getDate());
		shell.setLayout(new GridLayout());
		shell.setCursor(display.getSystemCursor(SWT.CURSOR_ARROW));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.addListener(SWT.Resize, __ -> {
            Rectangle rect = shell.getClientArea();
            Image newImage = new Image(display, Math.max(1, rect.width), 1);
            GC gc = new GC(newImage);
            gc.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
            gc.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
            gc.fillGradientRectangle(rect.x, rect.y, rect.width, 1, false);
            gc.dispose();
            shell.setBackgroundImage(newImage);
            if (oldBackImage != null) oldBackImage.dispose();
            oldBackImage = newImage;
        });
		topLevel = new Composite(shell, SWT.FILL);
		topLevel.setLayout(new GridLayout());
		topLevel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 0, 0));

		initTitle(topLevel);
		initBuild(topLevel);
		displaySeparation(topLevel);
		initApplicationName(topLevel);
		initApplicationVersion(topLevel);
		initApplicationDate(topLevel);
		initApplicationPath(topLevel);
		initIntroduction(topLevel);
		initIntroductionBis(topLevel);
		initLicenses(topLevel);
		initFreeLicenses(topLevel);
		initStartupScript(topLevel);
		initProgressBar(topLevel);
		initButtonBar(topLevel);

		// Go on
		shell.pack();
		shell.open();
		while (!shell.isDisposed())
			if (!display.readAndDispatch())
				display.sleep();
	}
	//--------------------------------------------------------------------------
	private static void initTitle(Composite parent) {
		Label title = new Label(parent, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		title.setText(appSetup.sTitle);
		title.setFont(fontTitle);
		title.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
	}

	private static void initBuild(Composite parent) {
		Composite compositeA2 = new Composite(parent, SWT.FILL | SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		compositeA2.setLayout(new GridLayout());
		compositeA2.setLayoutData(new GridData(SWT.FILL, SWT.BEGINNING, true, false));
		Label labA21 = new Label(compositeA2, SWT.CENTER);
		labA21.setText("Build " + appSetupBuildInfo.getNumber() + " - " + appSetupBuildInfo.getDate());
		labA21.setFont(fontBuild);
		labA21.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
	}

	private static void displaySeparation(Composite parent) {
		(new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL)).setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}
	//==== INITAPPLICATIONNAME =================================================
	private static void initApplicationName(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(3, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("Application / Archive name: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gdata = new GridData();
		gdata.grabExcessHorizontalSpace = true;
		gdata.horizontalAlignment = GridData.FILL;

		textName = new Text(area, SWT.BORDER);
		textName.setLayoutData(gdata);
		if (appSetup_UserApp.getName() != null)
			textName.setText(appSetup_UserApp.getName());

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Required field. Name of the self extractable archive.");
			}
		});

		textName.addModifyListener(¢ -> textName = (Text) ¢.widget);
	}
	//==== INITAPPLICATIONVERSION ==============================================
	private static void initApplicationVersion(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(3, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("Application version: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gdata = new GridData();
		gdata.grabExcessHorizontalSpace = true;
		gdata.horizontalAlignment = GridData.FILL;

		textVersion = new Text(area, SWT.BORDER);
		textVersion.setLayoutData(gdata);
		if (appSetup_UserApp.getVersion() != null)
			textVersion.setText(appSetup_UserApp.getVersion());

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Required field. Version of the product as character string." + appSetup.eol +
						"Examples: 1.3, Rev 4.5.2, Build 245");
			}
		});

		textVersion.addModifyListener(¢ -> textVersion = (Text) ¢.widget);
	}
	//==== INITAPPLICATIONDATE =================================================
	private static void initApplicationDate(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(3, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("Application date: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gdata = new GridData();
		gdata.grabExcessHorizontalSpace = true;
		gdata.horizontalAlignment = GridData.FILL;

		textDate = new Text(area, SWT.BORDER);
		textDate.setLayoutData(gdata);
		if (appSetup_UserApp.getDate() != null)
			textDate.setText(appSetup_UserApp.getDate());

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Required field. Date of this version of the product as character string." + appSetup.eol +
						"Examples: 02/09/2016, September 2016");
			}
		});

		textDate.addModifyListener(¢ -> textDate = (Text) ¢.widget);
	}
	//==== INITAPPLICATIONPATH =================================================
	private static void initApplicationPath(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(4, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("Application path: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gdata = new GridData();
		gdata.grabExcessHorizontalSpace = true;
		gdata.horizontalAlignment = GridData.FILL;

		textPath = new Text(area, SWT.BORDER);
		textPath.setLayoutData(gdata);
		if (appSetup_UserApp.getPath() != null)
			textPath.setText(appSetup_UserApp.getPath());

		Button open = new Button(area, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);
		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				DirectoryDialog dlg = new DirectoryDialog(shell, SWT.OPEN);
				dlg.setFilterPath(appSetup.homeDirectory);
				String fn = dlg.open();
				if (fn != null)
					textPath.setText(fn);
			}
		});

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Required field. Path to the product tree to compress.");
			}
		});

		textPath.addModifyListener(¢ -> textPath = (Text) ¢.widget);
	}
	//==== INITINTRODUCTION ====================================================
	private static void initIntroduction(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(5, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("Application introduction HTML file: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;

		txtIntroductionFile = new Text(area, SWT.BORDER);
		txtIntroductionFile.setLayoutData(gd);
		if (appSetup_Introduction.getFile() != null)
			txtIntroductionFile.setText(appSetup_Introduction.getFile());

		Button open = new Button(area, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);

		Button view = new Button(area, SWT.PUSH);
		view.setText("View");
		view.setFont(fontLabel);
		if (txtIntroductionFile.getText().length() == 0)
			view.setEnabled(false);
		view.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				if (txtIntroductionFile.getText().length() != 0)
					displayHTMLfile("Introduction", txtIntroductionFile.getText());
			}
		});

		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				FileDialog dlg = new FileDialog(shell, SWT.OPEN);
				dlg.setFilterNames(new String[] { ".html Files", ".htm Files", "All Files (*.*)", "All Files" });
				dlg.setFilterExtensions(new String[] { "*.html", "*.htm", "*.*", "*" });
				dlg.setFilterPath(dirforresearch);
				String fn = dlg.open();
				if (fn == null)
					return;
				txtIntroductionFile.setText(fn);
				File file = new File(fn);
				dirforresearch = file.getParentFile() + "";
				view.setEnabled(true);
				introCheck.setEnabled(true);
			}
		});

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Required field. HTML file describing the package and giving any information " +
				"of interest to the user");
			}
		});

		txtIntroductionFile.addModifyListener(¢ -> {
            txtIntroductionFile = (Text) ¢.widget;
            view.setEnabled(true);
            introCheck.setEnabled(true);
        });
	}
	//==== INITINTRODUCTIONBIS =================================================
	private static void initIntroductionBis(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(5, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("Files required to Introduction file: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;
		gd.heightHint = 100;
		txtIntroReferencedFiles = new Text(area, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		txtIntroReferencedFiles.setLayoutData(gd);
		if (appSetup_Introduction.getReferencedFiles() != null)
			Vertalign(appSetup_Introduction.getReferencedFiles());
		txtIntroReferencedFiles.addModifyListener(modifytxtIntroReferencedFiles);

		Button open = new Button(area, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);
		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				FileDialog dlg = new FileDialog(shell, SWT.OPEN | SWT.MULTI);
				dlg.setFilterNames(new String[] { "All Files (*.*)", "All Files" });
				dlg.setFilterExtensions(new String[] { "*.*", "*" });
				dlg.setFilterPath(dirforresearch);
				String fn = dlg.open();
				if (fn == null)
					return;
				dirforresearch = dlg.getFilterPath();
				if (dirforresearch.charAt(dirforresearch.length() - 1) != File.separatorChar)
					dirforresearch += File.separatorChar;
				String[] flist = dlg.getFileNames();
				List<String> res = new ArrayList<>();
				//for (String ¢ : txtIntroReferencedFiles.getText().split(appSetup.eol)) res.add(¢);
				Collections.addAll(res, txtIntroReferencedFiles.getText().split(appSetup.eol));
				for (String aFlist : flist) res.add(dirforresearch + aFlist);
				//for (int ¢ = 0; ¢ < flist.length; ++¢)
				//	res.add(dirforresearch + flist[¢]);
				String[] strarr = new String[res.size()];
				res.toArray(strarr);
				Vertalign(strarr);
			}
		});

		introCheck = new Button(area, SWT.PUSH);
		introCheck.setText("Check");
		introCheck.setFont(fontLabel);
		if (txtIntroductionFile.getText().length() == 0)
			introCheck.setEnabled(false);

		introCheck.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				if (txtIntroductionFile.getText().length() != 0)
					checkHTMLfile(txtIntroductionFile.getText(),
							txtIntroReferencedFiles.getText());
			}
		});

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Optional field. List of files referenced by the HTML 'introduction' file ");
			}
		});
	}
	//==== INITLICENSES ========================================================
	private static void initLicenses(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(5, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("User Software Licenses: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;

		txtLicensesFile = new Text(area, SWT.BORDER);
		txtLicensesFile.setLayoutData(gd);
		if (appSetup_Licenses.getFile() != null)
			txtLicensesFile.setText(appSetup_Licenses.getFile());

		Button open = new Button(area, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);

		Button view = new Button(area, SWT.PUSH);
		view.setText("View");
		view.setFont(fontLabel);
		if (txtLicensesFile.getText().length() == 0)
			view.setEnabled(false);
		view.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				if (txtLicensesFile.getText().length() != 0)
					displayHTMLfile("User Software Licenses", txtLicensesFile.getText());
			}
		});

		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				FileDialog dlg = new FileDialog(shell, SWT.OPEN);
				dlg.setFilterNames(new String[] { ".html Files", ".htm Files", "All Files (*.*)", "All Files" });
				dlg.setFilterExtensions(new String[] { "*.html", "*.htm", "*.*", "*" });
				dlg.setFilterPath(dirforresearch);
				String fn = dlg.open();
				if (fn == null)
					return;
				txtLicensesFile.setText(fn);
				File file = new File(fn);
				dirforresearch = file.getParentFile() + "";
				view.setEnabled(true);
			}
		});

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Optional field. The product may be shipped under the terms of licenses" +
						"contained in this HTML file. Acceptance of these licenses will be a preliminary to the " +
						"product installation.");
			}
		});

		txtLicensesFile.addModifyListener(¢ -> {
            txtLicensesFile = (Text) ¢.widget;
            view.setEnabled(true);
        });
	}
	//==== INITFREELICENSES ========================================================
	private static void initFreeLicenses(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(5, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("Free Software Licenses: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gd = new GridData();
		gd.grabExcessHorizontalSpace = true;
		gd.horizontalAlignment = GridData.FILL;

		txtFreeLicensesFile = new Text(area, SWT.BORDER);
		txtFreeLicensesFile.setLayoutData(gd);
		if (appSetup_Free.getFile() != null)
			txtFreeLicensesFile.setText(appSetup_Free.getFile());

		Button open = new Button(area, SWT.PUSH);
		open.setText("Open...");
		open.setFont(fontLabel);

		Button view = new Button(area, SWT.PUSH);
		view.setText("View");
		view.setFont(fontLabel);
		if (txtFreeLicensesFile.getText().length() == 0)
			view.setEnabled(false);
		view.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				if (txtFreeLicensesFile.getText().length() != 0)
					displayHTMLfile("Free Software Licenses", txtFreeLicensesFile.getText());
			}
		});

		open.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				FileDialog dlg = new FileDialog(shell, SWT.OPEN);
				dlg.setFilterNames(new String[] { ".html Files", ".htm Files", "All Files (*.*)", "All Files" });
				dlg.setFilterExtensions(new String[] { "*.html", "*.htm", "*.*", "*" });
				dlg.setFilterPath(dirforresearch);
				String fn = dlg.open();
				if (fn == null)
					return;
				txtFreeLicensesFile.setText(fn);
				File file = new File(fn);
				dirforresearch = file.getParentFile() + "";
				view.setEnabled(true);
			}
		});

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Optional field. The product may contain some GPL components or the product" +
						"itself may be shipped under a GPL license. This HTML file contains these licenses that " +
						"will be displayed to inform the user of this fact.");
			}
		});
		
		txtFreeLicensesFile.addModifyListener(¢ -> {
            txtFreeLicensesFile = (Text) ¢.widget;
            view.setEnabled(true);
        });
	}
	//==== INITSTARTUPSCRIPT ========================================================
	private static void initStartupScript(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(3, false));

		Label l1 = new Label(area, SWT.NONE);
		l1.setText("Startup script: ");
		l1.setFont(fontLabel);
		l1.setForeground(display.getSystemColor(SWT.COLOR_WHITE));

		GridData gdata = new GridData();
		gdata.grabExcessHorizontalSpace = true;
		gdata.horizontalAlignment = GridData.FILL;

		textStartup = new Text(area, SWT.BORDER);
		textStartup.setLayoutData(gdata);
		if (appSetup_UserApp.getStartup() != null)
			textStartup.setText(appSetup_UserApp.getStartup());

		Button help = new Button(area, SWT.PUSH);
		help.setText("Help");
		help.setFont(fontLabel);
		help.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				displayHelp("Optional field. Command to be executed from within the directory of extracted " +
						"files. Thus, if you wish to execute a program contained in this directory, you must " +
						"prefix your command with './'. For example, './program' will be fine.");
			}
		});
	}
	//==== INITPROGRESSBAR ==========================================================
	private static void initProgressBar(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		area.setLayout(new GridLayout(1, false));

		bar = new ProgressBar (area, SWT.SMOOTH);
		bar.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		bar.setMinimum(0);
		bar.setMaximum(1000);
	}
	//==== INITBUTTONBAR ============================================================
	private static void initButtonBar(Composite parent) {
		Composite buttonBar = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 6;
		layout.makeColumnsEqualWidth = true;
		buttonBar.setLayout(layout);

		final GridData data = new GridData(SWT.FILL, SWT.BOTTOM, true, false);
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = false;
		buttonBar.setLayoutData(data);

		Label labinvisible1 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible1.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible2 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible3 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible3.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible4 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible4.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

		cancel = new Button(buttonBar, SWT.PUSH);
		cancel.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, true, 1, 1));
		cancel.setText("Cancel");
		cancel.setFont(fontButton);
		cancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				appSetup.Exit(0);
			}
		});

		Button generate = new Button(buttonBar, SWT.PUSH);
		generate.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		generate.setText("Generate");
		generate.setFont(fontButton);
		generate.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				if (textName.getText().length() == 0) {
					displayWarning("Application / Archive name: information required!");
					return;
				}
				appSetup_UserApp.putName(textName.getText());

				if (textVersion.getText().length() == 0) {
					displayWarning("Application version: information required!");
					return;
				}
				appSetup_UserApp.putVersion(textVersion.getText());

				if (textDate.getText().length() == 0) {
					displayWarning("Application date: information required!");
					return;
				}
				appSetup_UserApp.putDate(textDate.getText());

				if (textPath.getText().length() == 0) {
					displayWarning("Application path: information required!");
					return;
				}
				String s = appSetup.normalizePath(textPath.getText());
				Path path = Paths.get(s);
				if (Files.notExists(path)) {
					displayWarning(path + ": not existent directory");
					return;
				}
				appSetup_UserApp.putPath(s);
				appSetup_UserApp.putSize(Long.valueOf(appSetupShellCmd.DU(s)));

				if (txtIntroductionFile.getText().length() == 0) {
					displayWarning("Application introduction HTML file: information required!");
					return;
				}
				s = appSetup.normalizePath(txtIntroductionFile.getText());
				path = Paths.get(s);
				if (Files.notExists(path)) {
					displayWarning("File '" + txtIntroductionFile.getText() + "' does not exist!");
					return;
				}
				appSetup_Introduction.putFile(s);
				appSetup_Introduction.putName(new File(s).getName());

				if (txtIntroReferencedFiles.getText().length() == 0)
					appSetup_Introduction.putReferencedFiles(null);
				else {
					File theFile = new File(appSetup_Introduction.getFile());
					String parent = theFile.getParent();
					String[] flist = txtIntroReferencedFiles.getText().split(appSetup.eol);					
					List<String> introfiles = new ArrayList<>();
					for (String aFlist : flist)
						if (aFlist.length() != 0) {
							String s1 = appSetup.normalizePath(aFlist);
							path = Paths.get(s1);
							if (Files.notExists(path)) {
								displayWarning("File '" + s1 + "' does not exist!");
								return;
							}
							theFile = new File(s1);
							if (!theFile.getParent().startsWith(parent))
								appSetup.Error("The file '" + s1 + "' does not belong to the tree whose root is '"
										+ appSetup_Introduction.getFile() + "'");
							introfiles.add(s1);
						}
					String[] strar = new String[introfiles.size()];
					introfiles.toArray(strar);
					appSetup_Introduction.putReferencedFiles(strar);
				}

				s = txtLicensesFile.getText();
				if (s.length() == 0)
					appSetup_Licenses.putName("");
				else {
					s = appSetup.normalizePath(s);
					appSetup_Licenses.putFile(s);
					appSetup_Licenses.putName(new File(s).getName());
					path = Paths.get(s);
					if (Files.notExists(path)) {
						displayWarning("File '" + txtLicensesFile.getText() + "' does not exist!");
						return;
					}
				}

				s = txtFreeLicensesFile.getText();
				if (s.length() == 0)
					appSetup_Free.putName("");
				else {
					s = appSetup.normalizePath(s);
					appSetup_Free.putFile(s);
					appSetup_Free.putName(new File(s).getName());
					path = Paths.get(s);
					if (Files.notExists(path)) {
						displayWarning("File '" + txtFreeLicensesFile.getText() + "' does not exist!");
						return;
					}
				}

				appSetup_UserApp.putStartup(textStartup.getText().length() == 0 ? "" : textStartup.getText());
				appSetupSelfExtractableArchive genThread = new appSetupSelfExtractableArchive();
				Cursor cursor = new Cursor(display, SWT.CURSOR_WAIT);
				shell.setCursor(cursor);
				genThread.start();

				for (int idx = 0, incr = DependingOnSize(appSetup_UserApp.getSize()); genThread.isAlive();) {
					idx += incr;
					if (idx <= bar.getMaximum())
						bar.setSelection(idx);
					topLevel.getParent().redraw();
					try {
						Thread.sleep(1000);
					} catch (Throwable ignored) { }
				}
				bar.setSelection(bar.getMaximum());
				shell.setCursor(display.getSystemCursor(SWT.CURSOR_ARROW));
				if (appSetupSelfExtractableArchive.ThreadStatus != null)
					appSetup.Error("Cannot create self extractable archive: " + appSetupSelfExtractableArchive.ThreadStatus);
				else {
					displayInformation(
							"Self extractable archive " + appSetup_UserApp.getName() + " successfully generated.");
					cancel.setText("Quit");
				}
			}
		});
	}
	//===============================================================================
	private static int DependingOnSize(long size) {
		return size < 200000000 ? 100 : size < 1000000000 ? 50 : size < 2000000000 ? 20 : 1;
	}
	//--------------------------------------------------------------------------
	static void Vertalign(String[] starr) {
		String txt = "";
		for (String ¢ : starr)
			if (txt.length() == 0)
				txt = ¢;
			else
				txt += appSetup.eol + ¢;
		txtIntroReferencedFiles.setText(txt);
	}
	//--------------------------------------------------------------------------
	private static void displayWarning(String s) {
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_ERROR);
		messageBox.setMessage(s);
		messageBox.open();
	}
	private static void displayInformation(String s) {
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_INFORMATION);
		messageBox.setMessage(s);
		messageBox.open();
		bar.setSelection(0);
	}
	private static void displayHelp(String s) {
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_INFORMATION);
		messageBox.setMessage(s);
		messageBox.open();
	}
	private static void displayHTMLfile(String title, String filename) {
		Shell shell = new Shell(display, SWT.CLOSE | SWT.TITLE);
		shell.setFont(fontLabel);
		shell.setText(title);
		shell.setLayout(new GridLayout());
		shell.setCursor(display.getSystemCursor(SWT.CURSOR_ARROW));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.addListener(SWT.Resize, __ -> {
            Rectangle rect = shell.getClientArea();
            Image newImage = new Image(display, Math.max(1, rect.width), 1);
            GC gc = new GC(newImage);
            gc.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
            gc.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
            gc.fillGradientRectangle(rect.x, rect.y, rect.width, 1, false);
            gc.dispose();
            shell.setBackgroundImage(newImage);
            if (oldBackImage != null) oldBackImage.dispose();
            oldBackImage = newImage;
        });

		GridData gridData = new GridData();
		gridData.verticalAlignment = gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = gridData.grabExcessHorizontalSpace = true;
		gridData.widthHint = 600;
		gridData.heightHint = 400;

		Browser browser = new Browser(shell, SWT.WRAP | SWT.MULTI | SWT.BORDER);
		browser.setLayoutData(gridData);
		browser.setUrl(filename);

		shell.pack();
		shell.open();
		while (!shell.isDisposed())
			if (!display.readAndDispatch())
				display.sleep();
	}
	//--------------------------------------------------------------------------
	private static boolean setCurrentDirectory(String directory_name) {
		boolean $ = false; 
		File directory = new File(directory_name).getAbsoluteFile();
		if (directory.exists() || directory.mkdirs())
			$ = (System.setProperty("user.dir", directory.getAbsolutePath()) != null);
		return $;
	}

	private static void checkHTMLfile(String htmlfile, String neededfilenames) {
		File theFile = new File(htmlfile);
		String parent = theFile.getParent();
		String file = theFile.getName();
		String target = appSetup.tmpdir + appSetup.slash + file;
		String tarballName = target + ".tar.gz" ;
		String content = file;
		if (neededfilenames.length() != 0)
			for (String ¢ : neededfilenames.split(appSetup.eol))
				if (¢.length() != 0)
					content += " " + ¢.replaceFirst(parent + "/", "");
		appSetupShellCmd.TAR(tarballName, parent, content);
		appSetupShellCmd.UNTAR(tarballName, appSetup.tmpdir);
		if (!setCurrentDirectory(appSetup.tmpdir))
			return;
		displayHTMLfile("Help to check completeness of document" + " - " + file, target);
		setCurrentDirectory(appSetup.currentDirectory);
	}
}
//==============================================================================
