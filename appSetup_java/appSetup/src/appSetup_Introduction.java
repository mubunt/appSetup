//------------------------------------------------------------------------------
/// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
class appSetup_Introduction {
    private static String introfile = "";	// Normalized pathname
    private static String nameIntroductionFile = "";	// Basename + suffix + ".tar.gz"
    private static String paddedNameIntroductionFile = "";
    private static String checksumIntroductionFile = "";
    private static long sizeIntroductionFile;
    private static long sizeExtractedIntroductionFile;
    private static String[] introfilelist;
    //--------------------------------------------------------------------------
    static String getFile() { return introfile; }
    static void putFile(String introducfile) { introfile = introducfile; }

    static String getName() { return nameIntroductionFile; }
    static void putName(String name) {
        nameIntroductionFile = name + appSetup.suffix_tar + appSetup.suffix_gzip;
        paddedNameIntroductionFile = appSetup.prefix_padded + nameIntroductionFile;
    }

    static String getBaseName() {
        return nameIntroductionFile.replace(appSetup.suffix_tar + appSetup.suffix_gzip, "");
    }

    static String getPaddedName() { return paddedNameIntroductionFile; }

    static String getChecksum() { return checksumIntroductionFile; }
    static void putChecksum(String checksum) { checksumIntroductionFile = checksum; }

    static long getSizeExtracted() { return sizeExtractedIntroductionFile; }
    static void putSizeExtracted(long bytes) { sizeExtractedIntroductionFile = bytes; }

    static long getSize() { return sizeIntroductionFile; }
    static void putSize(long ¢) { sizeIntroductionFile = ¢; }

    static String[] getReferencedFiles() { return introfilelist; }
    static void putReferencedFiles(String[] applicationintrofile) { introfilelist = applicationintrofile; }
}
//==============================================================================
