//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
class appSetup_Properties {
    private static String namePropertiesFile = "";
    private static String paddedNamePropertiesFile = "";
    private static String checksumPropertiesFile = "";
    private static long sizePropertiesFile;
    private static long sizeExtractedPropertiesFile;
    //--------------------------------------------------------------------------
    static String getName() { return namePropertiesFile; }
    static void putName() {
        namePropertiesFile = appSetup.namePropertiesFile + appSetup.suffix_tar + appSetup.suffix_gzip;
        paddedNamePropertiesFile = appSetup.prefix_padded + namePropertiesFile;
    }

    static String getPaddedName() { return paddedNamePropertiesFile; }

    static String getChecksum() { return checksumPropertiesFile; }
    static void putChecksum(String checksum) { checksumPropertiesFile = checksum; }

    static long getSizeExtracted() { return sizeExtractedPropertiesFile; }
    static void putSizeExtracted(long bytes) { sizeExtractedPropertiesFile = bytes; }

    static long getSize() { return sizePropertiesFile; }
    static void putSize(long ¢) { sizePropertiesFile = ¢; }
}
//==============================================================================
