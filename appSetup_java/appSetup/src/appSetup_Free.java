//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
class appSetup_Free {
    private static String freelicensesfile = "";
    private static String nameFreeLicenseFile = "";
    private static String paddedNameFreeLicenseFile = "";
    private static String checksumFreeLicenseFile = "";
    private static long sizeFreeLicenseFile;
    private static long sizeExtractedFreeLicenseFile;
    //--------------------------------------------------------------------------
    static String getFile() { return freelicensesfile; }
    static void putFile(String licfile) { freelicensesfile=licfile; }

    static String getName() { return nameFreeLicenseFile; }
    static void putName(String name) {
        if (name.length() == 0)
            nameFreeLicenseFile = paddedNameFreeLicenseFile = "";
        else {
            nameFreeLicenseFile = name +  appSetup.suffix_tar +  appSetup.suffix_gzip;
            paddedNameFreeLicenseFile =  appSetup.prefix_padded + nameFreeLicenseFile;
        }
    }

    static String getBaseName() { return nameFreeLicenseFile.replace( appSetup.suffix_tar +  appSetup.suffix_gzip, ""); }
    static String getPaddedName() { return paddedNameFreeLicenseFile; }

    static String getChecksum() { return checksumFreeLicenseFile; }
    static void putChecksum(String checksum) { checksumFreeLicenseFile = checksum; }

    static long getSizeExtracted() { return sizeExtractedFreeLicenseFile; }
    static void putSizeExtracted(long bytes) { sizeExtractedFreeLicenseFile = bytes; }

    static long getSize() { return sizeFreeLicenseFile; }
    static void putSize(long ¢) { sizeFreeLicenseFile = ¢; }
}
//==============================================================================
