//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
class appSetup_Jars {
    private static String nameJarsFile = "";
    private static String paddedNameJarsFile = "";
    private static String checksumJarsFile = "";
    private static long sizeJarsFile;
    private static long sizeExtractedJarsFile;
    //--------------------------------------------------------------------------
    static String getName() { return nameJarsFile; }
    static void putName() {
        nameJarsFile = appSetup.nameJarsFile + appSetup.suffix_tar + appSetup.suffix_gzip;
        paddedNameJarsFile = appSetup.prefix_padded + nameJarsFile;
    }

    static String getPaddedName() { return paddedNameJarsFile; }

    static String getChecksum() { return checksumJarsFile; }
    static void putChecksum(String checksum) { checksumJarsFile = checksum; }

    static long getSizeExtracted() { return sizeExtractedJarsFile; }
    static void putSizeExtracted(long bytes) { sizeExtractedJarsFile = bytes; }

    static long getSize() { return sizeJarsFile; }
    static void putSize(long bytes) { sizeJarsFile = bytes; }
}
//==============================================================================
