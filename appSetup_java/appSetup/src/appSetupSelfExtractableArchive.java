//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;

class appSetupSelfExtractableArchive extends Thread {
	static volatile String ThreadStatus;
	private final static String ltmpdir = appSetup.tmpdir;
	private final static String lslash = appSetup.slash;
	private final static String suffixSize = ".size";
	//--------------------------------------------------------------------------
	public void run() {
		appSetup.log("JARS archive generation.");
		String JarsPaddedTarballName = ltmpdir + lslash + appSetup_Jars.getPaddedName();
		try {
			jarFilesManagement(JarsPaddedTarballName);
		} catch (IOException e) { ThreadStatus = e.getMessage(); return; }
		appSetup.log(" DONE\n");

		appSetup.log("Properties archive generation.");
		String InfoPadedTarballName = ltmpdir + lslash + appSetup_Properties.getPaddedName();
		try {
			propertiesFileManagement(InfoPadedTarballName);
		} catch (IOException e) { ThreadStatus = e.getMessage(); return; }
		appSetup.log(" DONE\n");

		appSetup.log("Introduction archive generation.");
		String IntroPadedTarballName = ltmpdir + lslash + appSetup_Introduction.getPaddedName();
		try {
			introductionFileManagement(IntroPadedTarballName);
		} catch (IOException e) { ThreadStatus = e.getMessage(); return; }
		appSetup.log(" DONE\n");

		String LicensePadedTarballName = null;
		if (appSetup_Licenses.getName().length() != 0) {
			appSetup.log("License archive generation.");
			LicensePadedTarballName = ltmpdir + lslash + appSetup_Licenses.getPaddedName();
			try {
				licenseFileManagement(LicensePadedTarballName);
			} catch (IOException e) { ThreadStatus = e.getMessage(); return; }
			appSetup.log(" DONE\n");
		}

		String FreeLicensePadedTarballName = null;
		if (appSetup_Free.getName().length() != 0) {
			appSetup.log("Free License archive generation.");
			FreeLicensePadedTarballName = ltmpdir + lslash + appSetup_Free.getPaddedName();
			try {
				freeLicenseFileManagement(FreeLicensePadedTarballName);
			} catch (IOException e) { ThreadStatus = e.getMessage(); return; }
			appSetup.log(" DONE\n");
		}

		appSetup.log("Application archive generation.");
		String AppliPaddedTarballName = ltmpdir + lslash + appSetup_UserApp.getPaddedNameApplication();
		try {
			applicationFileManagement(AppliPaddedTarballName);
		} catch (IOException e) { ThreadStatus = e.getMessage(); return; }
		appSetup.log(" DONE\n");

		appSetup.log("Self extractable archive generation.");
		try {
			installationKitGeneration(JarsPaddedTarballName, InfoPadedTarballName,
				IntroPadedTarballName, LicensePadedTarballName,
				FreeLicensePadedTarballName, AppliPaddedTarballName);
		} catch (IOException e) { ThreadStatus = e.getMessage(); return; }
		appSetup.log(" DONE\n");
	}
	//--------------------------------------------------------------------------
	private static void jarFilesManagement(String PaddedTarballName) throws IOException {
		String tarballName = ltmpdir + lslash + appSetup_Jars.getName();
		long size = 0;
		for (String ¢ : appSetup.neededJarFiles.split(" "))
			size += new File(appSetup.InstallBinDir + ¢).length();
		appSetup_Jars.putSizeExtracted(size);
		appSetupShellCmd.TAR(tarballName, appSetup.InstallBinDir, appSetup.neededJarFiles);
		appSetup_Jars.putSize(Files.size(new File(tarballName).toPath()));
		appSetupShellCmd.DD(PaddedTarballName, tarballName, appSetup_Jars.getSize());
		appSetup_Jars.putChecksum(appSetupShellCmd.getMD5checksum(PaddedTarballName));
	}
	//--------------------------------------------------------------------------
	private static void propertiesFileManagement(String PaddedTarballName) throws IOException {
		String localName = ltmpdir + lslash + appSetup.namePropertiesFile;
		String tarballName = ltmpdir + lslash + appSetup_Properties.getName();
		appSetupProperties.generate(localName);
		appSetup_Properties.putSizeExtracted((new File(localName)).length());
		appSetupShellCmd.TAR(tarballName, ltmpdir, appSetup.namePropertiesFile);
		appSetup_Properties.putSize(Files.size(new File(tarballName).toPath()));
		appSetupShellCmd.DD(PaddedTarballName, tarballName, appSetup_Properties.getSize());
		appSetup_Properties.putChecksum(appSetupShellCmd.getMD5checksum(PaddedTarballName));
	}
	//--------------------------------------------------------------------------
	private static void introductionFileManagement(String PaddedTarballName) throws IOException {
		String tarballName = ltmpdir + lslash + appSetup_Introduction.getName();
		File theFile = new File(appSetup_Introduction.getFile());
		String parent = theFile.getParent();
		String fstr = theFile.getName();
		long size = theFile.length();
		String[] flist = appSetup_Introduction.getReferencedFiles();
		if (flist != null)
			for (String s : flist) {
				fstr += " " + s.replaceFirst(parent + "/", "");
				size += new File(s).length();
			}
		appSetup_Introduction.putSizeExtracted(size);
		appSetupShellCmd.TAR(tarballName, parent, fstr);
		appSetup_Introduction.putSize(Files.size(new File(tarballName).toPath()));
		appSetupShellCmd.DD(PaddedTarballName, tarballName, appSetup_Introduction.getSize());
		appSetup_Introduction.putChecksum(appSetupShellCmd.getMD5checksum(PaddedTarballName));
	}
	//--------------------------------------------------------------------------
	private static void licenseFileManagement(String PaddedTarballName) throws IOException {
		String localName = appSetup_Licenses.getFile();
		String tarballName = ltmpdir + lslash + appSetup_Licenses.getName();
		File theFile = new File(localName);
		String parent = theFile.getParent();
		String file = theFile.getName();
		appSetup_Licenses.putSizeExtracted(theFile.length());
		appSetupShellCmd.TAR(tarballName, parent, file);
		appSetup_Licenses.putSize(Files.size(new File(tarballName).toPath()));
		appSetupShellCmd.DD(PaddedTarballName, tarballName, appSetup_Licenses.getSize());
		appSetup_Licenses.putChecksum(appSetupShellCmd.getMD5checksum(PaddedTarballName));
	}
	//--------------------------------------------------------------------------
	private static void freeLicenseFileManagement(String PaddedTarballName) throws IOException {
		String localName = appSetup_Free.getFile();
		String tarballName = ltmpdir + lslash + appSetup_Free.getName();
		File theFile = new File(localName);
		String parent = theFile.getParent();
		String file = theFile.getName();
		appSetup_Free.putSizeExtracted(theFile.length());
		appSetupShellCmd.TAR(tarballName, parent, file);
		appSetup_Free.putSize(Files.size(new File(tarballName).toPath()));
		appSetupShellCmd.DD(PaddedTarballName, tarballName, appSetup_Free.getSize());
		appSetup_Free.putChecksum(appSetupShellCmd.getMD5checksum(PaddedTarballName));
	}
	//--------------------------------------------------------------------------
	private static void applicationFileManagement(String PaddedTarballName) throws IOException {
		String tarballName = ltmpdir + lslash + appSetup_UserApp.getNameApplication();
		appSetupShellCmd.TARDIR(tarballName, appSetup_UserApp.getPath());
		appSetup_UserApp.putSizeApplication(Files.size(new File(tarballName).toPath()));
		appSetupShellCmd.DD(PaddedTarballName, tarballName, appSetup_UserApp.getSizeApplication());
		appSetup_UserApp.putChecksumApplication(appSetupShellCmd.getMD5checksum(PaddedTarballName));
	}
	//--------------------------------------------------------------------------
	private static void installationKitGeneration(String jar, String info, String intro,
										String license, String freelicense, String appli) throws IOException {
		String prefix = appSetup.prefix;
		String scripttmp = ltmpdir + lslash + prefix + suffixSize;
		FileOutputStream outputStream = new FileOutputStream(scripttmp);
		generateHeader(outputStream, "00000000000000000000", "00000000000000000000", "00000000000000000000",
				"00000000000000000000", "00000000000000000000", "00000000000000000000");
		try { outputStream.close(); } catch (Exception ignored) { }

		String scripttmpPadded = ltmpdir + lslash + prefix + "-padded" + suffixSize;
		appSetupShellCmd.DD(scripttmpPadded, scripttmp, (new File(scripttmp)).length());

		long lskip1 = appSetupShellCmd.computeNumberBlocks((new File(scripttmpPadded)).length());
		long lskip2 = lskip1 + appSetupShellCmd.computeNumberBlocks(appSetup_Jars.getSize());
		long lskip3 = lskip2 + appSetupShellCmd.computeNumberBlocks(appSetup_Properties.getSize());
		long lskip4 = lskip3 + appSetupShellCmd.computeNumberBlocks(appSetup_Introduction.getSize());
		long lskip5 = lskip4 + appSetupShellCmd.computeNumberBlocks(appSetup_Licenses.getSize());
		long lskip6 = lskip5 + appSetupShellCmd.computeNumberBlocks(appSetup_Free.getSize());

		outputStream = new FileOutputStream(scripttmp, false);
		generateHeader(outputStream,
				String.format("%020d", lskip1),
				String.format("%020d", lskip2),
				String.format("%020d", lskip3),
				String.format("%020d", lskip4),
				String.format("%020d", lskip5),
				String.format("%020d", lskip6));
		outputStream.close();

		appSetupShellCmd.DD(appSetup_UserApp.getName(), scripttmp, (new File(scripttmp)).length());
		outputStream = new FileOutputStream(appSetup_UserApp.getName(), true);
		generateData(outputStream, jar, info, intro, license, freelicense, appli);
		try { outputStream.close(); } catch (Exception ignored) { }
		File f = new File(appSetup_UserApp.getName());
		if (! f.setExecutable(true))
			appSetup.Error("Cannot set file '" + f.getPath() + "' executable");
	}
	//--------------------------------------------------------------------------
	private static void generateHeader(FileOutputStream s,
			String skip1, String skip2, String skip3, String skip4, String skip5, String skip6) throws IOException {

		List<String> scriptPart01 = Arrays.asList(
			"#!/bin/bash",
			"##############################################################",
			"# This script was generated using AppSetup build " + appSetupBuildInfo.getNumber(),
			"##############################################################",
			"umask 077",
			"####----------------------------------------------------------",
			"#### Initializations",
			"####----------------------------------------------------------",
			"VERBOSE=1",
			"TAR_CMD=tar",
			"MD5_CMD=md5sum",
			"ECHO_CMD=echo",
			"DD_CMD=dd",
			"DF_CMD=df",
			"JAVA=java",
			"####----------------------------------------------------------",
			"INSTALLKIT=${0}",
			"HERE=$( pwd )",
			"TMPROOT=${TMPDIR:=/tmp}",
			"TMP=${TMPROOT}/appsetup.${RANDOM}",
			"BLOCKSIZE=" + appSetup.ddBlockSize,
			"",
			"NEEDED_DSKSPACE=" + ((appSetup_Free.getSizeExtracted() + appSetup_Jars.getSizeExtracted() + appSetup_UserApp.getSize() + appSetup_Licenses.getSizeExtracted()
					+ appSetup_Properties.getSizeExtracted() + appSetup_Introduction.getSizeExtracted()) / 1024 + 1),
			"JARS=" + appSetup_Jars.getName(),
			"JARSCHECKSUM=" + appSetup_Jars.getChecksum(),
			"INFO=" + appSetup_Properties.getName(),
			"INFOCHECKSUM=" + appSetup_Properties.getChecksum(),
			"INTRO=" + appSetup_Introduction.getName(),
			"INTROCHECKSUM=" + appSetup_Introduction.getChecksum(),
			"APPLI=" + appSetup_UserApp.getNameApplication(),
			"APPLICHECKSUM=" + appSetup_UserApp.getChecksumApplication()
			);

		List<String> scriptPart03 = Arrays.asList("",
			"####----------------------------------------------------------",
			"#### Ctrl-C trap",
			"####----------------------------------------------------------",
			"trap 'byebye -1' 1 2 3 4 6 8 10 12 13 15",
			"####----------------------------------------------------------",
			"#### Internal Functions",
			"####----------------------------------------------------------",
			"function Error {",
			"	${ECHO_CMD} \"!!! ERROR !!! $1. Abort ...\"",
			"	byebye 1",
			"}",
			"####----------------------------------------------------------",
			"function Exist { ",
			"	if [ ! -f $1 ]; then Error \"Wrong installation. File $1 does not exist\"; fi",
			"}",
			"####----------------------------------------------------------",
			"function testenvironment {",
			"	local s",
			"	[ \"x$DISPLAY\" = \"x\" ] && Error \"DISPLAY not initialized\"",
			"	s=$( java -version 2>&1 | grep \"version \\\"1.8\"  )",
			"	[[ \"x${s}\" == \"x\" ]] && Error \"'java' command does not refer to JAVA 8\"",
			"	s=`exec <&- 2>&-; which ${MD5_CMD} || type ${MD5_CMD}`",
			"	[[ \"x${s}\" == \"x\" ]] && Error \"Command '${MD5_CMD}' does not exist\"",
			"	s=`exec <&- 2>&-; which ${TAR_CMD} || type ${TAR_CMD}`",
			"	[[ \"x${s}\" == \"x\" ]] && Error \"Command '${TAR_CMD}' does not exist\"",
			"	s=`exec <&- 2>&-; which ${DD_CMD} || type ${DD_CMD}`",
			"	[[ \"x${s}\" == \"x\" ]] && Error \"Command '${DD_CMD}' does not exist\"",
			"	s=`exec <&- 2>&-; which ${DF_CMD} || type ${DF_CMD}`",
			"	[[ \"x${s}\" == \"x\" ]] && Error \"Command '${DF_CMD}' does not exist\"",
			"}",
			"####----------------------------------------------------------",
			"function getcompressedfiles {",
			"	ACTUALSIZE=${2}",
			"	SKIP=${3}",
			"	BLOCKS=`expr $ACTUALSIZE / $BLOCKSIZE`",
			"	REMAINDER=`expr $ACTUALSIZE % $BLOCKSIZE`",
			"	if [ ${REMAINDER:-0} -gt 0 ]; then BLOCKS=`expr $BLOCKS + 1`; fi",
			"	${DD_CMD} if=${INSTALLKIT} of=${1} bs=${BLOCKSIZE} count=${BLOCKS} skip=${SKIP} 2>/dev/null",
			"}",
			"####----------------------------------------------------------",
			"function checkchecksum {",
			"	local checksum",
			"	checksum=$( ${MD5_CMD} ${1} | cut -b-32 )",
			"	[ \"${2}\" == \"${checksum}\" ] || Error \"${3}: Wrong checksum\"",
			"}",
			"####----------------------------------------------------------",
			"function checkdiskspace {",
			"	local remain",
			"	remain=$( ${DF_CMD} -kP ${TMPROOT} | tail -1 | awk '{ print $4 }' )",
			"	[ ${remain} -gt ${NEEDED_DSKSPACE} ] || Error \"Not enough space left in '${TMPROOT}'' (${remain}KB). Needed ${NEEDED_DSKSPACE}KB\"",
			"}",
			"####----------------------------------------------------------",
			"function untar {",
			"	local untar",
			"	cd ${TMP}",
			"	${TAR_CMD} zxf ${1} 2>/dev/null",
			"	untar=$?",
			"	cd ${HERE}",
			"	[ $untar -ne 0 ] && Error \"Untar failed\"",
			"}",
			"####----------------------------------------------------------",
			"function launchjava {",
			"	cd ${TMP}",
			"	#### Consistency of the execution environment",
			"	Exist swt-48.jar",
			"	Exist commons-cli-1.4.jar",
			"	Exist commons-io-2.5.jar",
			"	Exist appSetupEmbd.jar",
			"	Exist " + appSetup_Properties.getName(),
			"	#### Go on...",
			"	${JAVA} -cp . -jar appSetupEmbd.jar ${internalOPTIONS}",
			"	cd ${HERE}",
			"}",
			"####----------------------------------------------------------",
			"function byebye {",
			"	[[ ${1} -eq -1 ]] && ${ECHO_CMD} \"CTRL-C trapped.\"",
			"	[ $VERBOSE ] && ${ECHO_CMD} \"Exiting...\"",
			"	rm -fr ${TMP}",
			"	exit ${1}",
			"}",
			"####----------------------------------------------------------",
			"#### Main",
			"####----------------------------------------------------------",
			"#### Argument parsing",
			"internalOPTIONS=",
			"while [[ $# -ge 1 ]]; do",
			"	case $1 in",
			"	-s|--silence) VERBOSE= ;;",
			"	--m*)",
			"		internalOPTIONS=\"${internalOPTIONS} $1\"",
			"		shift",
			"		if [[ $# -ge 1 ]]; then",
			"			case $1 in",
			"			-*)	Error \"Erroneous couple of options --m... $1\" ;;",
			"			*)	internalOPTIONS=\"${internalOPTIONS} $1\" ;;",
			"			esac",
			"		else",
			"			Error \"Erroneous parameter $1\"",
			"		fi",
			"		;;",
			"	-*|--*) Error \"Unknown option $1\" ;;",
			"    *) Error \"Unknown parameter $1\" ;;",
			"	esac",
			"	shift",
			"done",
			"#### Test system requirements",
			"[ $VERBOSE ] && ${ECHO_CMD} \"Ckecking system requirements...\"",
			"testenvironment",
			"#### Temporary directory creation",
			"mkdir -p ${TMP}",
			"#### Test if needed disk space available",
			"[ $VERBOSE ] && ${ECHO_CMD} \"Checking available disk space...\"",
			"checkdiskspace",
			"#### Get installation resources",
			"[ $VERBOSE ] && ${ECHO_CMD} \"Extracting the installation resources from the installer archive...\"",
			"getcompressedfiles ${TMP}/${JARS} " + appSetup_Jars.getSize() + " " + skip1,
			"getcompressedfiles ${TMP}/${INFO} " + appSetup_Properties.getSize() + " " + skip2,
			"getcompressedfiles ${TMP}/${INTRO} " + appSetup_Introduction.getSize() + " " + skip3
		);

		List<String> scriptPart05 = Arrays.asList(
			"getcompressedfiles ${TMP}/${APPLI} " + appSetup_UserApp.getSizeApplication() + " " + skip6,
			"#### Verifying archive integrity...",
			"[ $VERBOSE ] && ${ECHO_CMD} \"Verifying installation resources integrity...\"",
			"checkchecksum ${TMP}/${JARS} ${JARSCHECKSUM} \"JAR files\"",
			"checkchecksum ${TMP}/${INFO} ${INFOCHECKSUM} \"Properties file\"",
			"checkchecksum ${TMP}/${INTRO} ${INTROCHECKSUM} \"Introduction file\""
		);

		List<String> scriptPart07 = Arrays.asList(
			"checkchecksum ${TMP}/${APPLI} ${APPLICHECKSUM} \"Application file\"",
			"#### Untar installation resources, except application to be installed",
			"[ $VERBOSE ] && ${ECHO_CMD} \"Unpacking installation resources...\"",
			"untar ${JARS}",
			"untar ${INFO}",
			"untar ${INTRO}"
		);

		List<String> scriptPart09 = Arrays.asList(
			"#### Launch JAVA installer",
			"[ $VERBOSE ] && ${ECHO_CMD} \"Launching JAVA installer...\"",
			"launchjava",
			"####----------------------------------------------------------",
			"byebye 0",
			"####----------------------------------------------------------",
			"#### Installation Resources",
			"####----------------------------------------------------------"
		);

		// PART 1/9
		for (String l : scriptPart01) writeScriptLine(s, l);
		// PART 2/9
		if (appSetup_Licenses.getName().length() != 0) {
			writeScriptLine(s, "LICENSE=" + appSetup_Licenses.getName());
			writeScriptLine(s, "LICENSECHECKSUM=" + appSetup_Licenses.getChecksum());
		}
		if (appSetup_Free.getName().length() != 0) {
			writeScriptLine(s, "FREE=" + appSetup_Free.getName());
			writeScriptLine(s, "FREECHECKSUM=" + appSetup_Free.getChecksum());
		}
		// PART 3/9
		for (String l : scriptPart03) writeScriptLine(s, l);
		// PART 4/9
		if (appSetup_Licenses.getName().length() != 0)
			writeScriptLine(s, "getcompressedfiles ${TMP}/${LICENSE} " + appSetup_Licenses.getSize() + " " + skip4);
		if (appSetup_Free.getName().length() != 0)
			writeScriptLine(s, "getcompressedfiles ${TMP}/${FREE} " + appSetup_Free.getSize() + " " + skip5);
		// PART 5/9
		for (String l : scriptPart05) writeScriptLine(s, l);
		// PART 6/9
		if (appSetup_Licenses.getName().length() != 0)
			writeScriptLine(s, "checkchecksum ${TMP}/${LICENSE} ${LICENSECHECKSUM} \"License file\"");
		if (appSetup_Free.getName().length() != 0)
			writeScriptLine(s, "checkchecksum ${TMP}/${FREE} ${FREECHECKSUM} \"Free License file\"");
		// PART 7/9
		for (String l : scriptPart07) writeScriptLine(s, l);
		// PART 8/9
		if (appSetup_Licenses.getName().length() != 0)
			writeScriptLine(s, "untar ${LICENSE}");
		if (appSetup_Free.getName().length() != 0)
			writeScriptLine(s, "untar ${FREE}");
		// PART 9/9
		for (String l : scriptPart09) writeScriptLine(s, l);
	}
	//--------------------------------------------------------------------------
	private static void generateData(FileOutputStream s,
			String filedata1, String filedata2, String filedata3,
			String filedata4, String filedata5, String filedata6) throws IOException {
		bincat(s, filedata1);
		bincat(s, filedata2);
		bincat(s, filedata3);
		if (filedata4 != null) bincat(s, filedata4);
		if (filedata5 != null) bincat(s, filedata5);
		bincat(s, filedata6);
	}
	//--------------------------------------------------------------------------
	private static void bincat(FileOutputStream s, String filename) throws IOException {
		byte buffer[] = new byte[8192];
		int n, m = 0;

		InputStream inputstream = new FileInputStream(filename);
		while ((n = inputstream.read(buffer)) != -1) { s.write(buffer, 0, n); m = m + n; }
		inputstream.close();
	}
	//--------------------------------------------------------------------------
	private static void writeScriptLine(FileOutputStream outputStream, String s) throws IOException {
		outputStream.write((s + appSetup.eol).getBytes());
	}
}
//==============================================================================
