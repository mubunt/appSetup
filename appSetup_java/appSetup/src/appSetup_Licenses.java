//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
class appSetup_Licenses {
    private static String licensesfile = "";
    private static String nameLicenseFile = "";
    private static String paddedNameLicenseFile	 = "";
    private static String checksumLicenseFile = "";
    private static long sizeLicenseFile;
    private static long sizeExtractedLicenseFile;
    //--------------------------------------------------------------------------
    static String getFile() { return licensesfile; }
    static void putFile(String licensefile) { licensesfile = licensefile; }

    static String getName() { return nameLicenseFile; }
    static void putName(String name) {
        if (name.length() == 0)
            paddedNameLicenseFile = nameLicenseFile = "";
        else {
            nameLicenseFile = name +  appSetup.suffix_tar +  appSetup.suffix_gzip;
            paddedNameLicenseFile =  appSetup.prefix_padded + nameLicenseFile;
        }
    }

    static String getBaseName() {
        return nameLicenseFile.replace( appSetup.suffix_tar +  appSetup.suffix_gzip, "");
    }
    static String getPaddedName() { return paddedNameLicenseFile; }

    static String getChecksum() { return checksumLicenseFile; }
    static void putChecksum(String checksum) { checksumLicenseFile = checksum; }

    static long getSizeExtracted() { return sizeExtractedLicenseFile; }
    static void putSizeExtracted(long bytes) { sizeExtractedLicenseFile = bytes; }

    static long getSize() { return sizeLicenseFile; }
    static void putSize(long ¢) { sizeLicenseFile = ¢; }
}
//==============================================================================
