//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Text;

import java.util.ArrayList;
import java.util.List;

class appSetupIntroModif implements ModifyListener {
    @Override
    public void modifyText(ModifyEvent e) {
        List<String> res = new ArrayList<>();
        for (String ¢ : ((Text) e.widget).getText().split(appSetup.eol))
            if (¢.length() != 0)
                res.add(¢);
        String[] strarr = new String[res.size()];
        res.toArray(strarr);
        appSetupUI.txtIntroReferencedFiles.removeModifyListener(appSetupUI.modifytxtIntroReferencedFiles);
        appSetupUI.Vertalign(strarr);
        appSetupUI.txtIntroReferencedFiles.addModifyListener(appSetupUI.modifytxtIntroReferencedFiles);
    }
}
//==============================================================================