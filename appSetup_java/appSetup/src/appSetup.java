//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
import org.apache.commons.cli.*;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

class appSetup {
	static final String sName = "appSetup";
	private static final String jarFile = sName + ".jar";
	static final String sTitle = sName + " - An utility to generate self-extractable archives";
	static final String nameJarsFile = "appSetupEmbd.jar";
	static final String namePropertiesFile = "appSetupEmbd.properties";
	static final String nameApplicationFile	= "appSetupEmbd.application";
	static final String neededJarFiles = nameJarsFile + " commons-cli-1.4.jar commons-io-2.5.jar swt-48.jar";
	static final String suffixShell = ".sh";
	static final String prefix = sName;
	static final int ddBlockSize = 8192;
	static final String suffix_tar = ".tar";
	static final String suffix_gzip = ".gz";
	static final String prefix_padded = "Padded_";
	private static final String intelliJobjpath = "appSetup_java/appSetup/out/production/appSetup/";
	//--------------------------------------------------------------------------
	static final String eol = System.getProperty("line.separator");
	static final String slash = System.getProperty("file.separator");
	static final String currentDirectory = System.getProperty("user.dir");
	static final String homeDirectory = System.getProperty("user.home");
	private static final String tmp = System.getProperty("java.io.tmpdir");
	//--------------------------------------------------------------------------
	static String tmpdir;
	static String InstallBinDir;
	private static boolean graphicMode;
	private static boolean therewasthebatchopt;
	//==========================================================================
	public static void main(String[] args) {
		//---- PATH TO THIS FILE and path dependent initializations
		InstallBinDir = appSetup.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace(jarFile, "");
		if (InstallBinDir.length() > intelliJobjpath.length()) {
			if (intelliJobjpath.equals(InstallBinDir.substring(InstallBinDir.length() - intelliJobjpath.length()))) {
				InstallBinDir = InstallBinDir.substring(0, InstallBinDir.length() - intelliJobjpath.length() - 1) + slash + "appSetup_bin" + slash;
			}
		}
		//---- TEST OF THE CONSISTENCY OF THE EXECUTION CONTEXT
		for (String file : neededJarFiles.split(" ")) {
			File f = new File(InstallBinDir + file);
			if(! f.exists()) {
				Error("File " + InstallBinDir + file + " does not exist. Incorrect installation!");
				System.exit(-1);
			}
		}
		//---- INITIALIZATIONS
		appSetup_Jars.putName();
		appSetup_Properties.putName();
		appSetup_UserApp.putNameApplication();
		tmpdir = getTMPdir();
		//---- OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("h", "help",     false, "print this message and exit");
		options.addOption("V", "version",  false, "print the version information and exit");
		options.addOption("b", "batch",    false, "batch mode");
		options.addOption("A", "archive",  true,  "Archive name (result file)");
		options.addOption("R", "revision", true,  "Application revision");
		options.addOption("D", "date",     true,  "Application date");
		options.addOption("P", "path",     true,  "Application path");
		options.addOption("I", "intro",    true,  "Product introduction HTML file (main page)");
		options.addOption("J", "intro2",   true,  "Introduction secondary files (associated files)");
		options.addOption("F", "free",     true,  "Free license HTML file");
		options.addOption("L", "license",  true,  "User's license HTML file");
		options.addOption("S", "startup",  true,  "Startup script to be executed from within the directory of extracted files (Post-install command)");
		appSetup_UserApp.putStartup("");
		try {
			String s;
			CommandLine line = parser.parse(options, args);
			// Option: Help
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sName + " [OPTIONS]" + eol + "with OPTIONS:", options);
				Exit(0);
			}
			// Option: Version
			if (line.hasOption("version")) {
				System.out.println(sName + " - Build: "
						+ appSetupBuildInfo.getNumber() + " - Date: "
						+ appSetupBuildInfo.getDate());
				Exit(0);
			}
			// Option: Batch
			if (line.hasOption("batch"))
				therewasthebatchopt = true;
			// Option: Archive
			if (line.hasOption("archive"))
				appSetup_UserApp.putName(line.getOptionValue("archive"));
			// Option: Version
			if (line.hasOption("revision"))
				appSetup_UserApp.putVersion(line.getOptionValue("revision"));
			// Option: Date
			if (line.hasOption("date"))
				appSetup_UserApp.putDate(line.getOptionValue("date"));
			// Option: Path
			if (line.hasOption("path")) {
				s = normalizePath(line.getOptionValue("path"));
				appSetup_UserApp.putPath(s);
				Path path = Paths.get(s);
				if (Files.notExists(path))
					Error(path + ": not existent directory");
				appSetup_UserApp.putSize(Long.valueOf(appSetupShellCmd.DU(appSetup_UserApp.getPath())));
			}
			// Option: Intro
			if (line.hasOption("intro")) {
				s = normalizePath(line.getOptionValue("intro"));
				Path path = Paths.get(s);
				if (Files.notExists(path))
					Error(path + ": not existent file");
				appSetup_Introduction.putFile(s);
				appSetup_Introduction.putName(new File(s).getName());
			}
			// Option: Intro2
			if (line.hasOption("intro2")) {
				String[] flist = line.getOptionValue("intro2").split(",");
				String[] introfiles = new String[flist.length];
				for (int i = 0; i < flist.length; ++i ) {
					s = normalizePath(flist[i]);
					Path path = Paths.get(s);
					if (Files.notExists(path))
						Error(path + ": not existent file");
					introfiles[i] = s;
				}
				appSetup_Introduction.putReferencedFiles(introfiles);
			}
			// Option: License
			if (line.hasOption("license")) {
				s = normalizePath(line.getOptionValue("license"));
				Path path = Paths.get(s);
				if (Files.notExists(path))
					Error(path + ": not existent file");
				appSetup_Licenses.putFile(s);
				appSetup_Licenses.putName(new File(s).getName());
			}
			// Option: Free
			if (line.hasOption("free")) {
				s = normalizePath(line.getOptionValue("free"));
				Path path = Paths.get(s);
				if (Files.notExists(path))
					Error(path + ": not existent file");
				appSetup_Free.putFile(s);
				appSetup_Free.putName(new File(s).getName());
			}
			// Option: Startup
			if (line.hasOption("startup"))
				appSetup_UserApp.putStartup(line.getOptionValue("startup"));
		} catch (ParseException err) {
			Error("Parsing failed: " + err.getMessage());
		}
		if (!therewasthebatchopt) {
			graphicMode = true;
			appSetupUI.initUserInterface();
		} else {
			graphicMode = false;
			if (appSetup_UserApp.getName() == null)
				Error("Product / Archive name: information required!");
			if (appSetup_UserApp.getVersion() == null)
				Error("Product version: information required!");
			if (appSetup_UserApp.getDate() == null)
				Error("Product date: information required!");
			if (appSetup_UserApp.getPath() == null)
				Error("Product path: information required!");
			if (appSetup_Introduction.getFile() == null)
				Error("Product introduction HTML file: information required!");
			String[] introfiles = appSetup_Introduction.getReferencedFiles();
			if (introfiles.length != 0) {
				log("Introduction file set checking.");
				File theFile = new File(appSetup_Introduction.getFile());
				String parent = theFile.getParent();
				for (String ¢ : introfiles) {
					theFile = new File(¢);
					if (!theFile.getParent().startsWith(parent))
						Error("Path of '" + ¢ + "' inconsistent with the introduction file '"
								+ appSetup_Introduction.getFile() + "'");
				}
				log(" DONE\n");
			}
			appSetupSelfExtractableArchive genThread = new appSetupSelfExtractableArchive();
			genThread.start();
			try {
				Thread.sleep(1000);
			} catch (Throwable ignored) {}
			while (genThread.isAlive()) {
				System.out.print(".");
				try {
					Thread.sleep(2000);
				} catch (Throwable ignored) {}
			}
			System.out.println(
					eol + "Self extractable archive " + appSetup_UserApp.getName() + " successfully generated.");
		}
		//---- EXIT
		Exit(0);
	}
	//==========================================================================
	static void Error(String s) {
		if (!graphicMode)
			System.err.println("!!! ERROR !!! " + s);
		else {
			MessageBox messageBox = new MessageBox(Display.getCurrent().getActiveShell(), SWT.OK | SWT.ICON_ERROR);
			messageBox.setMessage(s);
			messageBox.open();
		}
		Exit(-1);
	}
	static void Exit(int value) {
		if (appSetupUI.oldBackImage != null) appSetupUI.oldBackImage.dispose();
		if (appSetupUI.fontTitle != null) appSetupUI.fontTitle.dispose();
		if (appSetupUI.fontBuild != null) appSetupUI.fontBuild.dispose();
		if (appSetupUI.fontLabel != null) appSetupUI.fontLabel.dispose();
		if (appSetupUI.fontButton != null) appSetupUI.fontButton.dispose();
		if (tmpdir != null) removeTMPdir(new File(tmpdir));
		if (appSetupUI.display != null) appSetupUI.display.dispose();
		System.exit(value);
	}
	private static String getTMPdir() {
		String $ = new File(tmp, new StringBuilder().append(prefix).append(UUID.randomUUID()) + "").getAbsolutePath();
		if (! new File($).mkdirs())
			Error("Cannot create temporary directory '" + $ + "'");
		return $;
	}
	private static void removeTMPdir(File d) {
		File[] contents = d.listFiles();
		if (contents != null)
			for (File ¢ : contents)
				removeTMPdir(¢);
		if (! d.delete())
			Error("Cannot delete temporary directory '" + d.toPath() + "'");
	}
	static String normalizePath(String f) {
		String s = f;
		if (s.charAt(0) == '/') return(s);
		if (s.charAt(0) != '~')
			s = FilenameUtils.normalize(appSetup.currentDirectory + "/" + s);
		else if (s.charAt(1) == '/')
			s = homeDirectory + slash + s.substring(2, s.length());
		return(s);
	}
	static void log(String ¢) {
		if (! appSetup.graphicMode) System.out.print(¢);
	}
}
//==============================================================================
