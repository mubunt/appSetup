//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//-------------------------------------------------------------------------------
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

class appSetupProperties {
	static void generate(String filename) {
		FileWriter write = null;
		try {
			write = new FileWriter(filename , false);
		} catch (IOException e) { appSetup.Error("Write error: " + e.getMessage()); }
		assert write != null;
		PrintWriter print = new PrintWriter(write);

		print.printf("AppSetupBuild=%d%s",		appSetupBuildInfo.getNumber(), appSetup.eol);
		print.printf("ApplicationName=%s%s",	appSetup_UserApp.getName(), appSetup.eol);
		print.printf("ApplicationVersion=%s%s",	appSetup_UserApp.getVersion(), appSetup.eol);
		print.printf("ApplicationDate=%s%s",	appSetup_UserApp.getDate(), appSetup.eol);
		print.printf("ApplicationTarball=%s%s",	appSetup_UserApp.getNameApplication(), appSetup.eol);
		print.printf("ApplicationSize=%s%s",	appSetup_UserApp.getSize(), appSetup.eol);
		print.printf("IntroductionFile=%s%s",	appSetup_Introduction.getBaseName(), appSetup.eol);
		if (appSetup_Licenses.getName() != null)
			print.printf("LicenseFile=%s%s",	appSetup_Licenses.getBaseName(), appSetup.eol);
		if (appSetup_Free.getName() != null)
			print.printf("FreeLicenseFile=%s%s",appSetup_Free.getBaseName(), appSetup.eol);
		if (appSetup_UserApp.getStartup() != null)
			print.printf("StartupCommand=%s%s",	appSetup_UserApp.getStartup(), appSetup.eol);
		print.close();
	}
}
//==============================================================================
