/*
 *******************************************************************************
 * Copyright (c) 2012 Original authors and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Original authors and others - initial API and implementation
 ******************************************************************************/
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;

/**
 * This class contains utility methods for drawing graphics
 * 
 * @see <a href="http://java-gui.info/Apress-The.Definitive.Guide.to.SWT.and.JFace/8886final/LiB0095.html">GC snippets</a>
 */
class GraphicsUtils {
	/*
	 * Creates an image containing the specified text, rotated either plus or minus
	 * 90 degrees.
	 * <dl>
	 * <dt><b>Styles: </b></dt>
	 * <dd>UP, DOWN</dd>
	 * </dl>
	 *
	 * @param text the text to rotate
	 * @param f the font to use
	 * @param foreground the color for the text
	 * @param background the background color
	 * @param style direction to rotate (up or down)
	 * @return Image
	 *          <p>
	 *          Note: Only one of the style UP or DOWN may be specified.
	 *          </p>
	 */
	static Image createRotatedText(String text, Font f, Color foreground,
		Color background, int style) {
		// Get the current display
		Display display = Display.getCurrent();
		if (display == null) SWT.error(SWT.ERROR_THREAD_INVALID_ACCESS);
		// Create a GC to calculate font's dimensions
		assert display != null;
		GC gc = new GC(display);
		gc.setFont(f);
		// Determine string's dimensions
		//    FontMetrics fm = gc.getFontMetrics();
		Point pt = gc.textExtent(text);
		// Dispose that gc
		gc.dispose();
		// Create an image the same size as the string
		Image stringImage = new Image(display, pt.x, pt.y);
		// Create a gc for the image
		gc = new GC(stringImage);
		gc.setFont(f);
		gc.setForeground(foreground);
		gc.setBackground(background);
		// Draw the text onto the image
		gc.drawText(text, 0, 0);
		// Draw the image vertically onto the original GC
		Image $ = createRotatedImage(stringImage, style);
		// Dispose the new GC
		gc.dispose();
		// Dispose the horizontal image
		stringImage.dispose();
		// Return the rotated image
		return $;
	}

	/*
	 * Creates a rotated image (plus or minus 90 degrees)
	 * <dl>
	 * <dt><b>Styles: </b></dt>
	 * <dd>UP, DOWN</dd>
	 * </dl>
	 *
	 * @param i the image to rotate
	 * @param style direction to rotate (up or down)
	 * @return Image
	 *          <p>
	 *          Note: Only one of the style UP or DOWN may be specified.
	 *          </p>
	 */
	private static Image createRotatedImage(Image i, int style) {
		// Get the current display
		Display display = Display.getCurrent();
		if (display == null) SWT.error(SWT.ERROR_THREAD_INVALID_ACCESS);
		// Use the image's data to create a rotated image's data
		ImageData sd = i.getImageData();
		ImageData dd = new ImageData(sd.height, sd.width, sd.depth, sd.palette);
		// Determine which way to rotate, depending on up or down
		boolean up = (style & SWT.UP) == SWT.UP;
		// Run through the horizontal pixels
		for (int sx = 0; sx < sd.width; ++sx)
			for (int sy = 0; sy < sd.height; ++sy)
				dd.setPixel((up ? sy : sd.height - sy - 1), (!up ? sx : sd.width - sx - 1), sd.getPixel(sx, sy));
		// Create the vertical image
		return new Image(display, dd);
	}  
}
