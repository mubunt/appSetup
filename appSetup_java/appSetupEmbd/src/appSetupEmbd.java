//------------------------------------------------------------------------------
// Copyright (c) 2016-2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// Project: appSetup
// A graphic utility to generate self-extractable archives
//
// Embedded part.
//-------------------------------------------------------------------------------
import org.apache.commons.cli.*;
import org.apache.commons.io.FilenameUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;

import java.io.*;
import java.nio.file.FileStore;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.UUID;

class appSetupEmbd {
	private static final String sName = "appSetupEmbd";
	// --------------------------------------------------------------------------
	private static final String[] screenTitles = {
			/* 0 */ "Introduction",
			/* 1 */ "License Agreement",
			/* 2 */ "License Agreement",
			/* 3 */ "Choose Installation Directory",
			/* 4 */ "Installation Summary",
			/* 5 */ "Installation"
			};
	// --------------------------------------------------------------------------
	private static final String propertyAppSetupBuild = "AppSetupBuild";
	private static final String propertyAppliName = "ApplicationName";
	private static final String propertyAppliVersion = "ApplicationVersion";
	private static final String propertyAppliDate = "ApplicationDate";
	private static final String propertyAppliTarball = "ApplicationTarball";
	private static final String propertyAppliSize = "ApplicationSize";
	private static final String propertyAppliStartup = "StartupCommand";
	private static final String propertyIntroFile = "IntroductionFile";
	private static final String propertyLicenseFile = "LicenseFile";
	private static final String propertyFreeFile = "FreeLicenseFile";
	// --------------------------------------------------------------------------
	private static String AppSetupBuild;
	private static String sAppliName;
	private static String sAppliVersion;
	private static String sAppliDate;
	private static String sAppliTarball;
	private static String sAppliSize;
	private static String sStartupCommand;
	private static String sIntroductionFile;
	private static String sLicenseFile;
	private static String sFreeLicenseFile;
	private static final String sPropertiesFileName	= "appSetupEmbd.properties";
	private static final int iScreens[] = new int[10];
	// --------------------------------------------------------------------------
	private static final String eol = System.getProperty("line.separator");
	private static final String userhomedir = System.getProperty("user.home");
	private static final String tmp = System.getProperty("java.io.tmpdir");
	private static final String currentDirectory = System.getProperty("user.dir");
	private static final String slash = System.getProperty("file.separator");
	private static final String homeDirectory = System.getProperty("user.home");
	// --------------------------------------------------------------------------
	private static final Font fontAppliName = new Font(Display.getDefault(), "Arial", 40, SWT.BOLD);
	private static final Font fontAppliVersion = new Font(Display.getDefault(), "Arial", 20, SWT.BOLD);
	private static final Font fontScreenTitle = new Font(Display.getDefault(), "Arial", 12, SWT.BOLD);
	private static final Font fontButton = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	private static final Font fontToolId = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	private static final Font fontButtonLicense = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	private static final Font fontLabel = new Font(Display.getDefault(), "Arial", 10, SWT.BOLD);
	private static final Font fontConsole = new Font(Display.getDefault(), "Arial", 10, SWT.NORMAL);
	private static final Font fontCommand = new Font(Display.getDefault(), "Courier", 10, SWT.NORMAL);

	private static Image oldBackImage;
	private static Image ititle;
	private static Image iversion;
	private static Group gConsole;
	private static Button next;
	private static Button previous;
	private static Button cancel;
	private static Button accept;
	private static Button donotaccept;
	private static Button choose;
	private static String installationDir;
	private static Display display;
	private static Shell shell;
	private static StyledText console;
	private static Browser browser;
	private static Label lbtLocation;
	private static Composite location;
	private static Text txtLocation;
	private static Boolean magic = false;
	private static final Color background = new Color(Display.getDefault(), 0, 0, 0);
	private static final Color consolebackground = new Color(Display.getDefault(), 255, 255, 255);
	private static int iScreen;
	private static final String[] tar_cmd = {
			"tar", "--verbose", "--gunzip", "--extract", "--directory", "%s", "--file", "%s"
			};
	// ==========================================================================
	public static void main(String[] args) {
		// ---- OPTIONS
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		// Working version of options....
		options.addOption("h", "help",      false, "print this message and exit");
		options.addOption("V", "version",   false, "print the version information and exit");
		options.addOption("",  "magic",     true,  "Application path");
		try {
			CommandLine line = parser.parse(options, args);
			// Option: Help
			if (line.hasOption("help")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp(sName
						+ " - Build: " + appSetupEmbdBuildInfo.getNumber() + " - Date: "
						+ appSetupEmbdBuildInfo.getDate(), options);
				Exit(0);
			}
			// Option: Version
			if (line.hasOption("version")) {
				System.out.println(sName + " - Build: "
						+ appSetupEmbdBuildInfo.getNumber() + " - Date: "
						+ appSetupEmbdBuildInfo.getDate());
				Exit(0);
			}
			// Option: Magic
			if (line.hasOption("magic")) {
				installationDir = normalizePath(line.getOptionValue("magic"));
				magic = true;
			}
		} catch (ParseException err) { Error("Parsing failed: " + err.getMessage()); }
		// ---- PROPERTIES
		getAppliProperties();
		if (!magic)
			initUserInterface();
		else
			installPackageMagic();
		// ---- EXIT
		Exit(0);
	}
	// ==========================================================================
	private static void getAppliProperties() {
		int idxScreens = 0;
		Properties properties = new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(sPropertiesFileName);
			properties.load(in);
			in.close();
		} catch (IOException err) { Error("Appli Information: " + err.getMessage()); }

		AppSetupBuild = properties.getProperty(propertyAppSetupBuild, null);
		sAppliName = properties.getProperty(propertyAppliName, null);
		sAppliVersion = properties.getProperty(propertyAppliVersion, null);
		sAppliDate = properties.getProperty(propertyAppliDate, null);
		sAppliTarball = properties.getProperty(propertyAppliTarball, null);
		sAppliSize = properties.getProperty(propertyAppliSize, null);
		sStartupCommand = properties.getProperty(propertyAppliStartup, null);
		sIntroductionFile = properties.getProperty(propertyIntroFile, null);
		iScreens[idxScreens] = 0; ++idxScreens;	// Introduction
		sLicenseFile = properties.getProperty(propertyLicenseFile, null);
		if (sLicenseFile.length() != 0) { iScreens[idxScreens] = 1; ++idxScreens; }
		sFreeLicenseFile = properties.getProperty(propertyFreeFile, null);
		if (sFreeLicenseFile.length() != 0) { iScreens[idxScreens] = 2; ++idxScreens; }
		iScreens[idxScreens] = 3; ++idxScreens;	// Location
		iScreens[idxScreens] = 4; ++idxScreens;	// Summary
		iScreens[idxScreens] = 5; ++idxScreens;	// Installation
	}

	// ==========================================================================
	private static void Error(String ¢) {
		System.err.println("!!! ERROR !!! " + ¢);
		Exit(-1);
	}

	private static void Exit(int value) {
		if (oldBackImage != null) oldBackImage.dispose();
		if (ititle != null) ititle.dispose();
		if (iversion != null) iversion.dispose();
		fontAppliName.dispose();
		fontAppliVersion.dispose();
		fontScreenTitle.dispose();
		fontConsole.dispose();
		fontButton.dispose();
		fontToolId.dispose();
		fontButtonLicense.dispose();
		fontLabel.dispose();
		if (display != null) display.dispose();
		System.exit(value);
	}

	private static void ErrorBox(String Message1, String Message2) {
		MessageBox messageBox = new MessageBox(shell, SWT.OK | SWT.ICON_ERROR);
		messageBox.setMessage(Message1 + ": " + Message2);
		messageBox.open();
	}
	// ==========================================================================
	private static void initUserInterface() {
		display = Display.getCurrent();
		shell = new Shell(display, SWT.MIN | SWT.CLOSE | SWT.TITLE | SWT.BORDER);
		shell.setText(sAppliName + " - Version: " + sAppliVersion + " - Date: " + sAppliDate);
		shell.setLayout(new GridLayout());
		shell.setCursor(display.getSystemCursor(SWT.CURSOR_ARROW));
		shell.setBackgroundMode(SWT.INHERIT_DEFAULT);
		shell.addListener(SWT.Resize, __ -> {
            Rectangle rect = shell.getClientArea();
            Image newImage = new Image(display, Math.max(1, rect.width), 1);
            GC gc = new GC(newImage);
            gc.setForeground(background);
            gc.setBackground(display.getSystemColor(SWT.COLOR_WHITE));
            gc.fillGradientRectangle(rect.x, rect.y, rect.width, 1, false);
            gc.dispose();
            shell.setBackgroundImage(newImage);
            if (oldBackImage != null) oldBackImage.dispose();
            oldBackImage = newImage;
        });
		Composite container = new Composite(shell, SWT.FILL);
		container.setLayout(new GridLayout());
		container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 0, 0));

		iScreen = 0;
		displayInstallationArea(container);
		displaySeparationArea(container);
		displayButtonArea(container);
		appSetupEmbdScreen.move(iScreens[iScreen]);

		// Go on
		shell.pack();
		shell.setSize(920, Math.max(570, ititle.getBounds().height + 190));
		shell.open();
		while (!shell.isDisposed())
			if (!display.readAndDispatch())
				display.sleep();
	}
	// ==========================================================================
	private static void displayInstallationArea(Composite parent) {
		Composite area = new Composite(parent, SWT.FILL);
		area.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 0, 0));
		GridLayout layout = new GridLayout(2, false);
		area.setLayout(layout);
		Composite c011 = new Composite(area, SWT.FILL);
		c011.setLayout(new GridLayout());
		c011.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 0, 0));
		Image ititletmp = GraphicsUtils.createRotatedText(sAppliName, fontAppliName,
				display.getSystemColor(SWT.COLOR_WHITE), background, SWT.UP);
		ImageData imageData = ititletmp.getImageData();
		imageData.transparentPixel = imageData.getPixel(0, 0);
		ititletmp.dispose();
		ititle = new Image(null, imageData);
		displayTitle(c011);
		Image iversiontmp = GraphicsUtils.createRotatedText(sAppliVersion + " - " + sAppliDate, fontAppliVersion,
				display.getSystemColor(SWT.COLOR_WHITE), background, SWT.UP);
		imageData = iversiontmp.getImageData();
		imageData.transparentPixel = imageData.getPixel(0, 0);
		ititletmp.dispose();
		iversion = new Image(null, imageData);
		displayVersion(c011);
		Composite c012 = new Composite(area, SWT.FILL);
		c012.setLayout(new GridLayout());
		c012.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 0, 0));

		gConsole = new Group(c012, SWT.NONE);
		gConsole.setFont(fontScreenTitle);
		gConsole.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
		gConsole.setText("  " + screenTitles[iScreens[iScreen]] + "  ");
		gConsole.setLayout(new GridLayout(1, false));
		gConsole.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 0, 0));
	}

	private static void displaySeparationArea(Composite parent) {
		Label separator1 = new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL);
		separator1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		Label tool = new Label(parent, SWT.HORIZONTAL);
		tool.setText("appSetup - build " + AppSetupBuild + " / " + sName
				+ " - build " + appSetupEmbdBuildInfo.getNumber());
		tool.setFont(fontToolId);
		tool.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
		(new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL)).setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}

	private static void displayButtonArea(Composite parent) {
		Composite buttonBar = new Composite(parent, SWT.FILL);
		GridLayout layout = new GridLayout();
		layout.numColumns = 6;
		layout.makeColumnsEqualWidth = true;
		buttonBar.setLayout(layout);

		final GridData data = new GridData(SWT.FILL, SWT.BOTTOM, true, false);
		data.grabExcessHorizontalSpace = true;
		data.grabExcessVerticalSpace = false;
		buttonBar.setLayoutData(data);

		cancel = new Button(buttonBar, SWT.PUSH);
		cancel.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, true, 1, 1));
		cancel.setText("Cancel");
		cancel.setFont(fontButton);

		Label labinvisible2 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible2.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible3 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible3.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));
		Label labinvisible4 = new Label(buttonBar, SWT.NO_FOCUS | SWT.HIDE_SELECTION);
		labinvisible4.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, true, 1, 1));

		previous = new Button(buttonBar, SWT.PUSH);
		previous.setLayoutData(new GridData(SWT.FILL, SWT.BOTTOM, true, true, 1, 1));
		previous.setText("Previous");
		previous.setFont(fontButton);
		previous.setEnabled(false);

		next = new Button(buttonBar, SWT.PUSH);
		next.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		next.setText("Next");
		next.setFont(fontButton);

		cancel.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				Exit(0);
			}
		});

		previous.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				next.setEnabled(true);
				--iScreen;
				if (iScreen == 0) previous.setEnabled(false);
				appSetupEmbdScreen.move(iScreens[iScreen]);
			}
		});

		next.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				previous.setEnabled(true);
				++iScreen;
				if (iScreen == iScreens.length - 1) next.setEnabled(false);
				if (iScreen < iScreens.length) appSetupEmbdScreen.move(iScreens[iScreen]);
			}
		});
	}

	private static void displayTitle(Composite parent) {
		parent.addPaintListener(¢ -> ¢.gc.drawImage(ititle, 0, (shell.getBounds().height - ititle.getBounds().height) / 3));
	}

	private static void displayVersion(Composite parent) {
		parent.addPaintListener(¢ -> ¢.gc.drawImage(iversion, ititle.getBounds().width,
                (shell.getBounds().height - iversion.getBounds().height) / 3));
	}

	private static void cleanComposites() {
		if (console != null) console.dispose();
		if (browser != null) browser.dispose();
		if (accept != null) accept.dispose();
		if (donotaccept != null) donotaccept.dispose();
		if (location != null) location.dispose();
		if (lbtLocation != null) lbtLocation.dispose();
		if (txtLocation != null) txtLocation.dispose();
		if (choose != null) choose.dispose();
	}
	// ==========================================================================
	static void ScreenIntroduction() {
		cleanComposites();
		gConsole.setText("  " + screenTitles[iScreens[iScreen]] + "  ");

		GridData gridData = new GridData();
		gridData.verticalAlignment = gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = gridData.grabExcessHorizontalSpace = true;
		gridData.widthHint = 600;
		gridData.heightHint = 400;

		browser = new Browser(gConsole, SWT.BORDER);
		browser.setLayoutData(gridData);

		browser.setUrl(str2url(sIntroductionFile));
		gConsole.layout();
	}
	// ==========================================================================
	static void ScreenLicense() {
		cleanComposites();
		gConsole.setText("  " + screenTitles[iScreens[iScreen]] + "  ");

		GridData gridData = new GridData();
		gridData.verticalAlignment = gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = gridData.grabExcessHorizontalSpace = true;
		gridData.widthHint = 600;
		gridData.heightHint = 400;

		browser = new Browser(gConsole, SWT.BORDER);
		browser.setLayoutData(gridData);

		Listener listenAgreement = ¢ -> {
            if (((Button) ¢.widget).getSelection())
                switch ((Integer) ¢.widget.getData()) {
                case 0:
                    next.setEnabled(true);
                    break;
                case 1:
                    next.setEnabled(false);
                    break;
                default:
                    break;
                }
        };

		accept = new Button(gConsole, SWT.RADIO);
		accept.setText("I accept the terms of the License Agreement");
		accept.setFont(fontButtonLicense);
		accept.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
		accept.setData(0);
		accept.addListener(SWT.Selection, listenAgreement);
		donotaccept = new Button(gConsole, SWT.RADIO);
		donotaccept.setText("I do NOT accept the terms of the License Agreement");
		donotaccept.setFont(fontButtonLicense);
		donotaccept.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
		donotaccept.setData(1);
		donotaccept.addListener(SWT.Selection, listenAgreement);
		next.setEnabled(false);

		browser.setUrl(str2url(sLicenseFile));
		gConsole.layout();
	}
	// ==========================================================================
	static void ScreenFreeLicense() {
		cleanComposites();
		gConsole.setText("  " + screenTitles[iScreens[iScreen]] + "  ");

		GridData gridData = new GridData();
		gridData.verticalAlignment = gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = gridData.grabExcessHorizontalSpace = true;
		gridData.widthHint = 600;
		gridData.heightHint = 400;

		browser = new Browser(gConsole, SWT.BORDER);
		browser.setLayoutData(gridData);
		browser.setUrl(str2url(sFreeLicenseFile));
		gConsole.layout();
	}
	// ==========================================================================
	static void ScreenLocation() {
		cleanComposites();
		gConsole.setText("  " + screenTitles[iScreens[iScreen]] + "  ");

		location = new Composite(gConsole, SWT.FILL);
		location.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		location.setLayout(new GridLayout(4, false));

		lbtLocation = new Label(location, SWT.NONE);
		lbtLocation.setText("Installation directory: ");
		lbtLocation.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
		lbtLocation.setFont(fontLabel);

		GridData dataProjectLocation = new GridData();
		dataProjectLocation.grabExcessHorizontalSpace = true;
		dataProjectLocation.horizontalAlignment = GridData.FILL;

		txtLocation = new Text(location, SWT.BORDER);
		txtLocation.setLayoutData(dataProjectLocation);
		if (installationDir == null)
			next.setEnabled(false);
		else {
			txtLocation.setText(installationDir);
			next.setEnabled(true);
		}
		txtLocation.addModifyListener(¢ -> {
            txtLocation = (Text) ¢.widget;
            installationDir = txtLocation.getText();
            next.setEnabled(true);
        });

		choose = new Button(location, SWT.PUSH);
		choose.setText("Choose...");
		choose.setFont(fontLabel);
		gConsole.layout();

		choose.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent __) {
				DirectoryDialog dlg = new DirectoryDialog(shell, SWT.OPEN);
				dlg.setFilterPath(userhomedir);
				String fn = dlg.open();
				if (fn == null)
					return;
				txtLocation.setText(fn);
				installationDir = txtLocation.getText();
				next.setEnabled(true);
			}
		});
		txtLocation.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent ¢) {
				if (¢.keyCode != SWT.CR)
					return;
				installationDir = txtLocation.getText();
				next.setEnabled(true);
			}
		});
	}
	// ==========================================================================
	static void ScreenPreinstallation() {
		cleanComposites();
		gConsole.setText("  " + screenTitles[iScreens[iScreen]] + "  ");

		console = new StyledText(gConsole, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		GridData gridData = new GridData();
		gridData.verticalAlignment = gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = gridData.grabExcessHorizontalSpace = true;
		console.setLayoutData(gridData);
		console.setBackground(consolebackground);
		console.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
		console.setFont(fontConsole);
		console.setEditable(false);
		console.setEnabled(true);
		gConsole.layout();

		NumberFormat nf = NumberFormat.getNumberInstance();
		FileStore store;
		String s = installationDir;
		Path path = Paths.get(s);
		if (Files.notExists(path)) {
			int n;
			while ((n = s.lastIndexOf(slash)) != -1) {
				s = s.substring(0, n);
				File f = new File(s);
				if (f.exists() && f.isDirectory())
					break;
			}
			if (n == -1) {
				ErrorBox("Cannot get a consistent name of a file system from", installationDir);
				Exit(-1);
			}
		}
		String available = null;
		try {
			store = Files.getFileStore(Paths.get(s));
			available = nf.format(store.getUsableSpace());
		} catch (IOException e) {
			ErrorBox("Cannot get available disk space amount", e.getMessage());
			Exit(-1);
		}

		printlnBoldConsole("Please, review the following before continuing:" + eol);
		printlnBoldConsole("	Application Name:");
		printlnConsole("		" + sAppliName + " version " + sAppliVersion + eol);
		printlnBoldConsole("	Installation Directory:");
		installationDir = normalizePath(installationDir);
		printlnConsole("		" + installationDir + eol);
		printlnBoldConsole("	Disk Space Information:");
		printlnConsole("		Required:" + sAppliSize + " bytes");
		printlnConsole("		Available:" + available + " bytes" + eol);
		printlnBoldConsole("	Startup script:");
		printlnConsole("\t\t" + (sStartupCommand.length() == 0 ? "none" : sStartupCommand + ""));
	}
	// ==========================================================================
	static void ScreenInstallation() {
		cleanComposites();
		gConsole.setText("  " + screenTitles[iScreens[iScreen]] + "  ");

		console = new StyledText(gConsole, SWT.WRAP | SWT.MULTI | SWT.BORDER | SWT.V_SCROLL);
		GridData gridData = new GridData();
		gridData.verticalAlignment = gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = gridData.grabExcessHorizontalSpace = true;
		gridData.minimumHeight = gridData.heightHint = Math.max(20 * console.getLineHeight(),
				ititle.getBounds().height + 20);
		gridData.minimumWidth = gridData.widthHint = 650; // ~ 80 chars
		console.setLayoutData(gridData);
		console.setBackground(consolebackground);
		console.setForeground(display.getSystemColor(SWT.COLOR_BLACK));
		console.setFont(fontConsole);
		console.setEditable(false);
		console.setEnabled(true);
		gConsole.redraw();

		Cursor cursor = new Cursor(display, SWT.CURSOR_WAIT);
		shell.setCursor(cursor);
		installPackage();
		shell.setCursor(display.getSystemCursor(SWT.CURSOR_ARROW));
		next.setEnabled(false);
		previous.setEnabled(false);
		cancel.setText("Done");
		gConsole.layout();
	}
	// ==========================================================================
	private static void installPackage() {
		Path path = Paths.get(installationDir);
		if (Files.notExists(path)) {
			printlnBoldConsole("Creating installation directory " + installationDir + "..." + eol);
			if (! new File(installationDir).mkdirs()) {
				ErrorBox("Cannot create directory '" + installationDir + "'", "");
				Exit(-1);
			}
		}

		printlnBoldConsole("Starting installation..." + eol);
		new Thread(new Runnable() {
			public void run() {
				try {
					String[] cmd = new String[tar_cmd.length];
					for (int ¢ = 0; ¢ < tar_cmd.length; ++¢)
						switch (¢) {
						case 5:
							cmd[¢] = installationDir;
							break;
						case 7:
							cmd[¢] = sAppliTarball;
							break;
						default:
							cmd[¢] = tar_cmd[¢];
							break;
						}
					executeCommand(cmd);
					if (sStartupCommand.length() != 0) {
						String[] tmpexecfile = { getTMPfile() };
						FileWriter write = null;
						try {
							write = new FileWriter(tmpexecfile[0], false);
						} catch (IOException e) {
							ErrorBox("Cannot create temporary file: ", e.getMessage());
							Exit(-1);
						}
						assert write != null;
						PrintWriter print = new PrintWriter(write);
						print.printf("#!/bin/bash%s", eol);
						print.printf("cd %s%s", installationDir.replaceAll(" ", "\\\\ "), eol);
						print.printf("%s%s", sStartupCommand, eol);
						print.close();
						File tmpf = new File(tmpexecfile[0]);
						if (! tmpf.setExecutable(true)) {
							ErrorBox("Cannot set executable file '" + tmpf.getPath()+ "'", "");
							Exit(-1);
						}
						display.asyncExec(() -> printlnBoldConsole(eol + "Running post-install script..." + eol));
						executeCommand(tmpexecfile);
						if (! tmpf.delete()) {
							ErrorBox("Cannot delete file '" + tmpf.getPath()+ "'", "");
							Exit(-1);
						}
					}
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					ErrorBox("Cannot execute a command:", e.getMessage());
					Exit(-1);
				}
				shell.getDisplay().asyncExec(() -> printlnBoldConsole(eol + "Press 'Done' to quit the installer." + eol + eol));
			}

			void executeCommand(String[] command) {
				try {
					ProcessBuilder pb = (new ProcessBuilder(command)).redirectErrorStream(true);
					Process p = pb.start();
					InputStream is = p.getInputStream();
					InputStreamReader isr = new InputStreamReader(is);
					BufferedReader br = new BufferedReader(isr);
					String line;
					while ((line = br.readLine()) != null) {
						if (line.charAt(0) == '.' && line.charAt(1) == '/')
							line = line.substring(2, line.length());
						final String s = line;
						display.asyncExec(() -> printlnCourierConsole(s));
					}
				} catch (IOException e) {
					display.asyncExec(() -> {
						ErrorBox("Cannot execute command: ", e.getMessage());
						printlnConsoleErr("\t" + "!!! ERROR !!! " + e.getMessage());
					});
				}
			}
		}).start();
	}

	private static void installPackageMagic() {
		Path path = Paths.get(installationDir);
		if (Files.notExists(path)) {
			System.out.println("Creating installation directory " + installationDir + "...");
			if (! new File(installationDir).mkdirs()) {
				ErrorBox("Cannot create directory '" + installationDir + "'","");
				Exit(-1);
			}
		}

		System.out.println("Starting installation...");
		String[] cmd = new String[tar_cmd.length];
		for (int ¢ = 0; ¢ < tar_cmd.length; ++¢)
			switch (¢) {
			case 5:
				cmd[¢] = installationDir;
				break;
			case 7:
				cmd[¢] = sAppliTarball;
				break;
			default:
				cmd[¢] = tar_cmd[¢];
				break;
			}
		executeCommandMagic(cmd);
		System.out.println("Done");
	}

	private static void executeCommandMagic(String[] command) {
		try {
			ProcessBuilder pb = new ProcessBuilder(command);
			pb = pb.redirectErrorStream(true);
			Process p = pb.start();
			InputStream is = p.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;
			while ((line = br.readLine()) != null) {
				if (line.length() != 0) {
					if (line.charAt(0) == '.' && line.charAt(1) == '/')
						line = line.substring(2, line.length());
					System.out.println("\t" + line);
				}
			}
		} catch (IOException e) {
			System.out.println("!!! ERROR !!! " + e.getMessage());
		}
	}
	// ==========================================================================
	private static void printlnConsole(String label) {
		if (magic)
			System.out.println(label);
		else {
			console.append(label + eol);
			console.setSelection(console.getText().length());
		}
	}
	private static void printlnConsoleErr(String label) {
		StyleRange styleRange = new StyleRange();
		styleRange.fontStyle = SWT.BOLD;
		styleRange.foreground = Display.getDefault().getSystemColor(SWT.COLOR_RED);
		styleRange.start = console.getCharCount();
		styleRange.length = label.length();
		printlnConsole(label);
		console.setStyleRange(styleRange);
	}
	private static void printlnBoldConsole(String label) {
		StyleRange styleRange = new StyleRange();
		styleRange.fontStyle = SWT.BOLD;
		styleRange.start = console.getCharCount();
		styleRange.length = label.length();
		printlnConsole(label);
		console.setStyleRange(styleRange);
	}
	private static void printlnCourierConsole(String label) {
		StyleRange styleRange = new StyleRange();
		styleRange.font = fontCommand;
		styleRange.start = console.getCharCount();
		styleRange.length = label.length();
		printlnConsole(label);
		console.setStyleRange(styleRange);
	}
	private static String getTMPfile() {
		return new File(tmp,
				new StringBuilder().append(sName).append(LocalDateTime.now()).append(UUID.randomUUID()) + "")
						.getAbsolutePath();
	}
	private static String normalizePath(String f) {
		String s = f;
		if (s.charAt(0) == '/')
			return (s);
		if (s.charAt(0) != '~')
			s = FilenameUtils.normalize(currentDirectory + "/" + s);
		else if (s.charAt(1) == '/')
			s = homeDirectory + slash + s.substring(2, s.length());
		return (s);
	}
	private static String str2url(String f) {
		return("file://" + new File(f).getAbsolutePath());
	}
}
// ==============================================================================
