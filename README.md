
# APPLICATION: *appSetup*,  graphic utility to generate self-extractable archives

**appSetup** is a graphic utility running on UBUNTU^[1](#fn1){#ref1}^) that generates a self-extractable compressed tar archive from a directory. The resulting file appears as a shell script, and can be launched as is. The archive will then uncompress itself via a temporary directory and an optional arbitrary command will be executed (for example an installation script). This is pretty similar to archives generated with "makeself". **appSetup** archives also include checksums for integrity self-validation (MD5 checksums). Installation of the compress software is graphic too.

The installation process is as a series of screens, the user from one to the other by issuing, as appropriate, the requested information and clicking the "Next" button. It may, at any time but prior to launching the actual installation, return to the previous screen and make a correction, or abandon the process.

The serie of screens is as follows:

- *Introduction*: HTML text describing the package.
- *License Agreement*:List of licenses to which the package is subject. It can have two lists, one that the user must accept, one that informs the User. Traditionally, we will find "proprietary"  licenses in the first list, and GPL licenses and associated, in the second.
- *Choose Installation Directory*: The user specifies the path to the installation directory. A browser is at his disposal to select this directory.
- *Installation Summary*: Identification of the package, location of installation, size needed for installation on disk versus the available size, post-installation command.
- *Installation*: Log of the installation.

**appSetup** has been tested on the following platforms :

- UBUNTU 16.04, ..., 18.04

## USAGE appSetup

``` bash
$ appSetup --help
appSetup - Copyright (c) 2016-2018, Michel RIZZO. All Rights Reserved.
appSetup - Version 1.8.0

An utility to generate self-extractable archives

Usage: appSetup [OPTIONS]...

  -h, --help              Print help and exit
  -V, --version           Print version and exit
  -b, --batch             Batch mode. Graphic mode by default)  (default=off)
  -A, --archive=FILENAME  Archive name (result file)
  -R, --revision=STRING   Application revision
  -D, --date=STRING       Application date
  -P, --path=PATH         Application path
  -I, --intro=FILENAME    Application introduction HTML file
  -J, --intro2=FILENAME   Introduction secondary files (associated files)
  -L, --license=FILENAME  User's license HTML file
  -F, --free=FILENAME     Free license HTML file
  -S, --startup=STRING    Startup script to be executed from within the
                            directory of extracted files
$ 
```

*archive*
:   Name of the archive (and so, the package) to be created.

*revision*
:   Revision/version identification of the package. Examples: 1.3, Rev
    4.5.2, Build 245.

*date*
:   Date identification of the package. Examples: 02/09/2016,
    September 2016.

*path*
:   Name of the directory that contains the files to be archived.

*intro*
:   An arbitrary HTML text describing the package. It will be displayed
    at the beginning.

*intro2*
:   Comma-separated list of files referenced by the introduction HTML
    file (css, images, icons, etc.).

*license*
:   The set of HTML "commercial" licenses that the user has to agree
    before installing the package. Acceptance of these licenses will be
    a preliminary to the package.

*free*
:   The package may contain some GPL components or the product itself
    may be shipped under a GPL license. This HTML file contains these
    licenses that will be displayed to inform the user of this fact.

*startup*
:   Command to be executed \_from within\_ the directory of
    extracted files. Thus, if you wish to execute a program contain in
    this directory, you must prefix your command with "./". For example,
    ./program will be fine.

Example:

``` bash
$ appSetup ==> Refer to the screenshot below. 
$ 
```

![appSetup](README_images/appSetup-1.png  "appSetup")

## USAGE shell script (self-extractable archives)

	$ script [ --silence | -s ]         `

*silence*
:   Silence mode

Example:

``` bash
$ ./appSetup
Ckecking system requirements... 
Checking available disk space... 
Extracting the installation resources from the installer archive... 
Extracting JARS files...
Extracting Properties file... 
Extracting Application file... 
Verifying installation resources integrity... 
Unpacking JAR files... 
Unpacking Properties files...
Launching JAVA installer... ==> Refer to screenshots below.Exiting...
$         `
```
[![appSetup](README_images/appSetup-2.png "README_images/appSetup-2.png")

[![appSetup](README_images/appSetup-3.png "README_images/appSetup-3.png")

[![appSetup](README_images/appSetup-4.png "README_images/appSetup-4.png")

[![appSetup](README_images/appSetup-5.png "README_images/appSetup-5.png")

[![appSetup](README_images/appSetup-6.png "README_images/appSetup-6.png")

### LICENSE

**appSetup** is covered by the GNU General Public License (GPL) version 3 and above. Archives generated by **appSetup** don't have to be placed under this license, since the archive itself is merely data for **appSetup**.

## STRUCTURE OF THE APPLICATION

This section walks you through appSetup's structure. Once you understand this structure, you will easily find your way around in appSetup's code base.

``` bash
$ yaTree 
./                                                           # Application level
├── README_images/                                           # Images for documentation
│   ├── appSetup-1.png                                       # -- Screenshot appSetup
│   ├── appSetup-2.png                                       # -- Screenshot appSetup
│   ├── appSetup-3.png                                       # -- Screenshot appSetup
│   ├── appSetup-4.png                                       # -- Screenshot appSetup
│   ├── appSetup-5.png                                       # -- Screenshot appSetup
│   └── appSetup-6.png                                       # -- Screenshot appSetup
├── appSetup_bin/                                            # Binary directory: jars (third-parties and local) and driver
│   ├── Makefile                                             # -- Makefile
│   ├── commons-cli-1.4.jar                                  # -- Third-party COMMONS CLI 1.4 jar file
│   ├── commons-io-2.5.jar                                   # -- Third-party COMMONS IO 2.5 jar file
│   └── swt-48.jar                                           # 
├── appSetup_c/                                              # C Source directory
│   ├── Makefile                                             # -- Makefile
│   ├── appSetup.c                                           # -- C main source file (application driver)
│   └── appSetup.ggo                                         # -- 'gengetopt' option definition
│                                                            # -- Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── appSetup_java/                                           # JAVA Source directory
│   ├── appSetup/                                            # -- IntelliJ appSetup project structure
│   │   └── ...                                              # 
│   ├── appSetupEmbd/                                        # -- IntelliJ appSetupEmbd project structure
│   │   └── ...                                              # 
│   ├── Makefile                                             # -- General makefile for build
│   ├── appSetup.mk                                          # -- Makefile for appSetup  build
│   └── appSetupEmbd.mk                                      # -- Makefile for appSetupEmbd build
├── tests/                                                   # Some ressources to quiclky test the application
│   ├── INTRO.css                                            # -- CSS file, companion of INTRO.html
│   ├── INTRO.html                                           # -- CSS file, companion of INTRO.html
│   ├── INTRO.png                                            # -- PNG file, companion of INTRO.html
│   ├── LICENSE.html                                         # -- License file
│   └── Makefile                                             # -- Makefile to produce archive via command-line
├── COPYING.md                                               # GNU General Public License markdown file
├── LICENSE.md                                               # License markdown file
├── Makefile                                                 # Top-level makefile
├── README.md                                                # ReadMe Mark-Down file
├── RELEASENOTES.md                                          # Release Notes markdown file
└── VERSION                                                  # Version identification text file

9 directories, 46 files
```

## HOW TO BUILD AND INSTALL THIS APPLICATION

	$ cd appSetup 
	$ make all
	$ make release INSTALLDIR=......
	$ appSetup         # assuming that the installation directory is defined in your PATH env variable         `

## HOW TO PLAY WITH JAVA SOURCES

To play with, we recommend to use **IntelliJ IDEA**, on Linux platform:

- Launch IntelliJ IDEA
- Click on **File -> Open...** and select *appSetup/appSetup_java/appSetup*  or *appSetup/appSetup_java/appSetupEmbd* project.
- It's your turn...

## SYSTEM REQUIREMENTS

- For usage:
	- Linux computer
	- Linux utilities: tar, md5sum, dd and df.
	- JAVA 1.8.0

- For development:
	- Linux computer
	- Linux utilities: tar, md5sum, dd and df.
	- IntelliJ IDEA 2018.2.3 Build #IC-182.4323.46, built on September 3, 2018
	- JRE: 1.8.152-release-1248-b8 amd64
	- JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
	- GNU gengetopt 2.22.6 

## NOTES

- Based on work by Stephane Peter (makeself-2.2.0) (megastep at megastep.org)

##RELEASE NOTES

Refer to file [RELEASENOTES](RELEASENOTES.md).

***
